package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ClickonCLOSEButtonBlueprintCreatedPopup {
  @Test
  public void ClickonCLOSEButtonBlueprintCreatedPopupDASHBOPARD() throws Exception
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1000);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
        System.out.println(driver.getTitle());
		Thread.sleep(1000);
		Reporter.log("Browser Session Maximized");
	    System.out.println(driver.getTitle());
		Thread.sleep(1000);
		Reporter.log("Browser Maximized");
		driver.findElement(By.id(BpConstants.CLICK_ON_CREATEBUTTON_DASHBOARD_BUTTON)).click();
		Thread.sleep(1000);
		Reporter.log("Click on Create Button");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.id("newBlueprintPopup_newName_Input")).sendKeys("BluePrint212");
	    Thread.sleep(1000);
		Reporter.log("Enter the BP Name");
	    System.out.println(driver.getTitle());
		driver.findElement(By.id(BpConstants.CLICK_ON_CREATE_BUTTON)).click();
		Thread.sleep(1000);
		Reporter.log("Click On Create Button");
		driver.findElement(By.className(BpConstants.CLICK_ON_CLOSE_BUTTON_DASHBOARDCREATEDBP_POPUP)).click();
	    System.out.println(driver.getTitle());
		Reporter.log("Click on CLOSE Button");
	    driver.close();
		Reporter.log("Browser Session Closed");
	    driver.quit();
	  
  }
 

}
