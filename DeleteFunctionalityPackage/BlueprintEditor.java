package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class BlueprintEditor {
  @Test
  public void clickonTOSCAEditor() throws Exception
  {

	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Thread.sleep(1550);
		driver.findElement(By.xpath("//div[@class='item active'][normalize-space()='TOSCA']")).click();
	    System.out.println(driver.getTitle());
		Thread.sleep(1550);
		Reporter.log("click on tosca header");
		driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
		Thread.sleep(1550);
		Reporter.log("click on template editer");
		JavascriptExecutor js = (JavascriptExecutor)driver;		
        js.executeScript("window.scrollBy(0,6000)");			
	    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
		Thread.sleep(1550);
		Reporter.log("Click on New Button from SLIDEBAR");
		driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
		Thread.sleep(1550);
		Reporter.log("Click on Browse  Button from SLIDEBAR");
		driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[4]/button[1]")).click();
		Thread.sleep(1550);
		Reporter.log("Click on UploadButton from SLIDEBAR");
		driver.close();
		driver.quit();

				
				
  }
  

}
