package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class CreateBluePrintFunctionality 
{
	 @Test(priority =1)
	  public void CreateablueprintwithexistingnamefromSlidebar() throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_ENTERNAME_BPNAME)).sendKeys("BlueprintToBeDeleted");
			Thread.sleep(1550);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
			driver.findElement(By.cssSelector(BpConstants.SLIDE_CREATE_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on SLIDE BAR CREATE BUTTON");
			driver.close();
		    driver.quit();
			
		  		  
	  }
	 @Test(priority =14)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete21() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='BlueprintToBeDeleted']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority = 2)
	  public void CreateBluePrintWithInvalidNameDashboard() throws Exception 
	  {

		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.id(BpConstants.CLICK_ON_CREATEBUTTON_DASHBOARD_BUTTON)).click();
			Thread.sleep(1050);
			Reporter.log("Click on Create Button");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id("newBlueprintPopup_newName_Input")).sendKeys("XYZ123!@#$");
		    Thread.sleep(1050);
			Reporter.log("Enter the BP Name");	    
		    driver.close();
		    driver.quit();
		  
	  }
	 @Test(priority =3)
	  public void CREATEBPDASHBOARD() throws Exception 
	  {

		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.id(BpConstants.CLICK_ON_CREATEBUTTON_DASHBOARD_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Create Button");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id("newBlueprintPopup_newName_Input")).sendKeys("BluePrintTry");
		    Thread.sleep(1550);
			Reporter.log("Enter the BP Name");
		    System.out.println(driver.getTitle());
			driver.findElement(By.id(BpConstants.CLICK_ON_CREATE_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click On Create Button");
			Thread.sleep(1550);
			driver.findElement(By.id("newBlueprintPopup_Cancel_OpenCreated_Button")).click();
			Thread.sleep(2000);
			
		    System.out.println(driver.getTitle());
			Reporter.log("Click on Delete icon ");
		    driver.close();
		    driver.quit();		
	  }
	 @Test(priority =15)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete22() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='BluePrintTry']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority =4)
	  public void CREATENEWBLUEPRINTFROMQUICKHELP() throws Exception 
	  {

		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			    System.out.println(driver.getTitle());
			
			Reporter.log("Browser is maximized in Internet Explorer");
			driver.findElement(By.id(BpConstants.CREATEBP_LINK_QUICKHELP)).click();
			Thread.sleep(1050);
			Reporter.log("Click on the Create Blueprint Button");
			driver.findElement(By.id("newBlueprintPopup_newName_Input")).sendKeys("SampleBlueprint");
			Thread.sleep(1050);
			Reporter.log("Enter the Create Blueprint Name");
			driver.findElement(By.id("newBlueprintPopup_Create_Button")).click();
			Thread.sleep(4550);
			Reporter.log("Click on the Create Blueprint Button");
			driver.findElement(By.id("newBlueprintPopup_OpenCreated_Button")).click();
			Thread.sleep(4550);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[1]/div[1]")).click();
			Reporter.log("Click on the Dashboard");
			Thread.sleep(3000);
			driver.close();
			Reporter.log("Browser Session in Internet Explorer is Closed");
			driver.quit();
			Reporter.log("Browser  Session in Internet Explorer is Quit");



	  }
	 @Test(priority =16)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete23() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='SampleBlueprint']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority = 5)
	  public void CreateNewBlueprintrecordfromSlidebar() throws Exception
	  {

		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session Maximized");
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_ENTERNAME_BPNAME)).sendKeys("CreateBlueprintToBeDeleted");
			Thread.sleep(1550);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
			driver.findElement(By.cssSelector(BpConstants.SLIDE_CREATE_BUTTON)).click();
			Thread.sleep(2550);
			Reporter.log("Click on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[1]/div[1]")).click();
			Reporter.log("Click on the Dashboard");
			Thread.sleep(3000);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
			Reporter.log("Browser Session Quit");

	  }		
	 @Test(priority =17)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete1() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='CreateBlueprintToBeDeleted']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
		 @Test(priority = 6)
	  public void CreateBlueprintfromSlideBarWithHalfInput() throws Exception
	  {

			 FileInputStream fi = new FileInputStream("config.properties");
				Properties prop= new Properties();
				prop.load(fi);
				System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
				WebDriver driver = new InternetExplorerDriver();
				driver.get(prop.getProperty("baseURL"));
				Thread.sleep(1000);
				Reporter.log("Browser Session Started");
			    driver.manage().window().maximize();
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[1]/div[1]")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[1]/input")).sendKeys("CreateBlueprintToBeDeleted1");
			Thread.sleep(1000);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath("//button[@class='custom-btn btn-primary']")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR CREATE BUTTON");
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[1]/div[1]")).click();
			Reporter.log("Click on the Dashboard");
			Thread.sleep(1000);
		    System.out.println(driver.getTitle());
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
			Reporter.log("Browser Session Quit");

	  }		 
		 @Test(priority =18)
		  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete12() throws Exception 
		  {
			  FileInputStream fi = new FileInputStream("config.properties");
				Properties prop= new Properties();
				prop.load(fi);
				System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
				WebDriver driver = new InternetExplorerDriver();
				driver.get(prop.getProperty("baseURL"));
				Thread.sleep(1550);
				Reporter.log("Browser Session Started");
			    driver.manage().window().maximize();
			    System.out.println(driver.getTitle());
				Thread.sleep(1550);
				Reporter.log("Browser Maximized");
				driver.findElement(By.xpath("//td[normalize-space()='CreateBlueprintToBeDeleted1']/following-sibling::td/span[@class='table-btn-delete']")).click();
			    System.out.println(driver.getTitle());
				Reporter.log("Click on delete Button");
				driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
			    System.out.println(driver.getTitle());
				Reporter.log("Click on delete Button");
				driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
			    System.out.println(driver.getTitle());
				Reporter.log("Click on okay Button");
				driver.close();
				Reporter.log("Browser Session Closed");
			    driver.quit();
		  }
	 @Test(priority = 7)
	  public void CreateBlueprintfromSlideBarWithInvalidInput() throws Exception
	  {

		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session Maximized");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[1]/div[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[1]/input")).sendKeys("@3$%^%0");
			Thread.sleep(1550);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
			Reporter.log("Browser Session Quit");

	  }		 
	 @Test(priority = 8)
	  public void CreateBlueprintfromSlideBarWithMaximumInputs() throws Exception
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[1]/div[1]")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[1]/input")).sendKeys("dferrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
			Thread.sleep(1000);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath("//button[@class='custom-btn btn-primary']")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR CREATE BUTTON");
			Thread.sleep(1000);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
			Reporter.log("Browser Session Quit");

	  }		 
	 @Test(priority =19)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete123() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='dferrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority = 9)
	  public void CreateBlueprintfromSlideBarWithManimumInputs() throws Exception
	  {

		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[1]/div[1]")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[1]/input")).sendKeys("Gff");
			Thread.sleep(1000);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath("//button[@class='custom-btn btn-primary']")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR CREATE BUTTON");
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
			Reporter.log("Browser Session Quit");

	  }
	 @Test(priority =20)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete1234() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='Gff']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority = 10)
	  public void CreateBlueprintfromSlideBarWithNumericInputs() throws Exception
	  {

		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[1]/div[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[1]/input")).sendKeys("55555555555");
			Thread.sleep(1550);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath("//button[@class='custom-btn btn-primary']")).click();
			Thread.sleep(2550);
			Reporter.log("Click on SLIDE BAR CREATE BUTTON");
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
			Reporter.log("Browser Session Quit");

	  }		
	 @Test(priority =21)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete12345() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='55555555555']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority = 11)
	  public void CreateBlueprintfromSlideBarWithAlphaNumericInputs() throws Exception
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[1]/div[1]")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR NEW BUTTON");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[1]/input")).sendKeys("ABCD5555");
			Thread.sleep(1000);
			Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath("//button[@class='custom-btn btn-primary']")).click();
			Thread.sleep(1000);
			Reporter.log("Click on SLIDE BAR CREATE BUTTON");
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
			Reporter.log("Browser Session Quit");

	  }		 
	 @Test(priority =22)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete123456() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='ABCD5555']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	  
	
	
}