package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ClickonBlueprintFunctionality 
{
 
//	 @Test(priority =0)
//	  public void ClickontheBlueprintRecordDashboard() throws Exception 
//	  {
//		 FileInputStream fi = new FileInputStream("config.properties");
//			Properties prop= new Properties();
//			prop.load(fi);
//			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
//			WebDriver driver = new InternetExplorerDriver();
//			driver.get(prop.getProperty("baseURL"));
//			Thread.sleep(1550);
//			Reporter.log("Browser Session Started");
//		    driver.manage().window().maximize();
//	        System.out.println(driver.getTitle());
//			Reporter.log("Browser Session maximized");
//			Thread.sleep(1000);
//			System.out.println(driver.getTitle());
//		    driver.findElement(By.xpath("//td[normalize-space()='gourav_ka_blueprint']")).click();
//		    Thread.sleep(10550);
//			Reporter.log("Click on the BpRecord1 dashboard");
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(10550);
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_RETURNTO_DASHBOARD)).click();
//		    Thread.sleep(10550);
//			Reporter.log("Click on the Returnto dashboard Tab");
//		    System.out.println(driver.getTitle());
//		    //Thread.sleep(10550);
//		    //driver.findElement(By.xpath("/html/body/app-root/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/tbody/tr[3]/td[1]")).click();
//		   // Thread.sleep(10550);
//			//Reporter.log("Click on the BpRecord2 dashboard");
//			//Thread.sleep(10550);
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(10550);    
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_ZOOMBUTTON_BPEDITOR)).click();
//		    System.out.println(driver.getTitle());
//			Reporter.log("Click on the ZOOMBUTTON dashboard");
//			Thread.sleep(10550); 
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_ZOOMOUTBUTTON_BPEDITOR)).click();
//		    System.out.println(driver.getTitle());
//			Reporter.log("Click on the ZOOMBUTTON dashboard");
//			Thread.sleep(10550); 
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_AUTOZOOMBUTTON_BPEDITOR)).click();
//		    System.out.println(driver.getTitle());
//			Reporter.log("Click on the ZOOMBUTTON dashboard");
//			Thread.sleep(10550); 
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_AUTODRAWBUTTON_BPEDITOR)).click();
//		    System.out.println(driver.getTitle());
//			Reporter.log("Click on the ZOOMBUTTON dashboard");
//			driver.close();
//			Reporter.log("Browser Session Closed");
//			driver.quit();
//
//			
//
//		  
//	  }
	 @Test(priority =1)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboard() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.id(BpConstants.CLICK_ON_CREATEBUTTON_DASHBOARD_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Create Button");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id("newBlueprintPopup_newName_Input")).sendKeys("sss");
		    Thread.sleep(1550);
			Reporter.log("Enter the BP Name");
		    System.out.println(driver.getTitle());
			driver.findElement(By.id(BpConstants.CLICK_ON_CREATE_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click On Create Button");
			driver.findElement(By.xpath("//button[@id='newBlueprintPopup_Cancel_OpenCreated_Button']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on CLOSE Button");
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority =7)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='sss']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority =8)
	  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete1() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='ssss']/following-sibling::td/span[@class='table-btn-delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on delete Button");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on okay Button");
			driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 
	
	 @Test(priority =2)
	  public void ClickonCancelButtonfromCloneBpdashboard() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.xpath("//td[normalize-space()='TOSCA Editor Demo']/following-sibling::td/span[@class='table-btn-copy']")).click();
			Thread.sleep(1550);
			Reporter.log("Click on Clone Button");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id(BpConstants.CLICK_ON_CANCEL_BUTTON_DASHBOARD_POPUP2)).click();
		    Thread.sleep(1550);
			Reporter.log("Click on Cancel");
		    System.out.println(driver.getTitle());
		    driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
		  
	  }
	 
	 @Test(priority = 3)
	  public void ClickonCLOSEButtonBlueprintCreatedPopupDASHBOPARD() throws Exception
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Maximized");
			driver.findElement(By.id(BpConstants.CLICK_ON_CREATEBUTTON_DASHBOARD_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Create Button");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id("newBlueprintPopup_newName_Input")).sendKeys("ssss");
		    Thread.sleep(1550);
			Reporter.log("Enter the BP Name");
		    System.out.println(driver.getTitle());
			driver.findElement(By.id(BpConstants.CLICK_ON_CREATE_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click On Create Button");
			driver.findElement(By.className(BpConstants.CLICK_ON_CLOSE_BUTTON_DASHBOARDCREATEDBP_POPUP)).click();
		    System.out.println(driver.getTitle());
			Reporter.log("Click on CLOSE Button");
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
		  
   }
	 @Test(priority = 4)
	  public void CLICKONHELPLINK() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session Maximized");
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_HELP_LINK)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Help Link");
			driver.findElement(By.className(BpConstants.SLIDEBAR_HELPLINK_CLOSE)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Help Close Button");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();


			

		  
	  }
//	 @Test(priority = 5)
//	  public void ClickontheBlueprintRecordDashboard1() throws Exception 
//	  {
//		  FileInputStream fi = new FileInputStream("config.properties");
//			Properties prop= new Properties();
//			prop.load(fi);
//			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
//			WebDriver driver = new InternetExplorerDriver();
//			driver.get(prop.getProperty("baseURL"));
//			Thread.sleep(1550);
//			Reporter.log("Browser Session Started");
//		    driver.manage().window().maximize();
//		    System.out.println(driver.getTitle());
//			Thread.sleep(1550);
//			Reporter.log("Browser Session Maximized");
//		    System.out.println(driver.getTitle());
//		    //driver.findElement(By.xpath(BpConstants.CLICK_ON_CREATEDBPRECORD1_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the BpRecord1 dashboard");
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(1550);
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_RETURNTO_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the Returnto dashboard Tab");
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(1550);
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_CREATEDBPRECORD2_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the BpRecord2 dashboard");
//			Thread.sleep(1550);
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(1550);
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_RETURNTO_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the Returnto dashboard Tab");
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_CREATEDBPRECORD3_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the BpRecord3 dashboard");
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(1550);
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_RETURNTO_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the Returnto dashboard Tab");
//		    System.out.println(driver.getTitle());
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_CREATEDBPRECORD4_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the BpRecord4 dashboard");
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(1550);
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_RETURNTO_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the Returnto dashboard Tab");
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_CREATEDBPRECORD5_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the BpRecord5 dashboard");
//		    System.out.println(driver.getTitle());
//		    Thread.sleep(1550);
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_RETURNTO_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the Returnto dashboard Tab");
//		    driver.findElement(By.xpath(BpConstants.CLICK_ON_CREATEDBPRECORD6_DASHBOARD)).click();
//		    Thread.sleep(1550);
//			Reporter.log("Click on the BpRecord6 dashboard");
//			Thread.sleep(1550);
//			driver.findElement(By.xpath(BpConstants.CLICK_ON_ZOOMBUTTON_BPEDITOR)).click();
//			Thread.sleep(1550);
//			Reporter.log("Click on the ZoomIN BPEDITOR");
//			Thread.sleep(1550);
//			driver.findElement(By.xpath(BpConstants.CLICK_ON_ZOOMOUTBUTTON_BPEDITOR)).click();
//			Thread.sleep(1550);
//			Reporter.log("Click on the ZoomOut BPEDITOR");
//			Thread.sleep(1550);
//			driver.findElement(By.xpath(BpConstants.CLICK_ON_AUTOZOOMBUTTON_BPEDITOR)).click();
//			Thread.sleep(1550);
//			Reporter.log("Click on the AUTOZoom BPEDITOR");
//			Thread.sleep(1550);
//		    System.out.println(driver.getTitle());
//			driver.findElement(By.xpath(BpConstants.CLICK_ON_AUTODRAWBUTTON_BPEDITOR)).click();
//			Thread.sleep(1550);
//			Reporter.log("Click on the AutoDrawZoom BPEDITOR");		
//			driver.findElement(By.xpath(BpConstants.CLICK_ON_RETURNTO_DASHBOARD)).click();
//			Thread.sleep(1550);
//		    System.out.println(driver.getTitle());
//		    Reporter.log("Click on the Returnto dashboard Tab");		
//			driver.close();
//			Reporter.log("Browser Session Closed");
//			driver.quit();
//			
//		  
//	  }
	 @Test(priority = 6)
	  public void ClickonWeekdbuttonDashboard() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			Thread.sleep(1550);
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_LASTWEEK_ICON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Lastweeks link");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_LAST30DAYS_ICON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Last30days link");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_LAST6MONTHS_ICON)).click();
			Reporter.log("Click on Last6monthsLink");
			Thread.sleep(1550);
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();

		  
	  }
	 
	 
	 
	 
	 
	 
}
