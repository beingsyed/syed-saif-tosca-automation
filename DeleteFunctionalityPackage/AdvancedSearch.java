package DeleteFunctionalityPackage;


import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class AdvancedSearch {
  @Test
  public void AdvancedsearchfromDashboard() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Thread.sleep(1550);
		Reporter.log("Browser Session Maximized");
		driver.findElement(By.id(BpConstants.ENTER_TEXT_SEARCH_TEXTBOX)).sendKeys("Blue");
		Thread.sleep(1550);
		Reporter.log("Enter the text");
		driver.findElement(By.xpath("//i[@class='fas fa-search']")).click();
		Thread.sleep(1050);
		Reporter.log("Click on SEARCH Button");
	    System.out.println(driver.getTitle());
		Thread.sleep(1550);
	    driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
		Thread.sleep(1550);
		Reporter.log("Click on Maximize Button");
		driver.findElement(By.name(BpConstants.SELECT_FROM_DATE)).sendKeys("01/01/2019");
		Thread.sleep(1550);
		Reporter.log("Enter the FROMDATE");
		driver.findElement(By.name(BpConstants.SELECT_TO_DATE)).sendKeys("17/12/2019");
		Thread.sleep(1550);
		Reporter.log("Enter the TODATE");
	    driver.findElement(By.xpath("//*[@id=\"advSerch_Filter_bpAuthorSelect\"]/div/span[2]")).click();
		Thread.sleep(1550);
		Reporter.log("SELECT the Author");
	    driver.findElement(By.xpath("//div[@class='ng-option ng-option-marked ng-star-inserted']")).click();
	    Thread.sleep(1550);
		Reporter.log("SELECT the Author name from drop down box");
		Thread.sleep(1550);
		driver.findElement(By.xpath("//button[@class='btn btn-primary filter-apply-btn'][normalize-space()='Apply']")).click();
		Thread.sleep(1550);
		Reporter.log("Click on Apply Button");
	    System.out.println(driver.getTitle());
		Thread.sleep(1550);
	    driver.findElement(By.xpath("/html/body/app-root/div/div/div/div/app-search-blueprint/div/div[2]/div[2]/div[1]/app-dashboardtable/div/table/thead/tr/th[1]/i")).click();
		Thread.sleep(1550);
		Reporter.log("Click on Selected Record");
	    System.out.println(driver.getTitle());
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();
	  
  }
 

}
