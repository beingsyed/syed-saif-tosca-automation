package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class input17 {

	 @Test(priority =1)
	  public void create_and_open_blueprint_from_dashboard() throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(3000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    driver.findElement(By.xpath("//i[@class='fas fa-file-medical']")).click();
			Thread.sleep(3000);
			Reporter.log("Click on create blueprint");
			driver.findElement(By.xpath("//input[@id='newBlueprintPopup_newName_Input']")).click();
			Thread.sleep(3000);
			Reporter.log("Click on textbox to create blueprint");
			driver.findElement(By.xpath("//input[@id='newBlueprintPopup_newName_Input']")).sendKeys("blueprintforinput");
			Thread.sleep(3000);
			Reporter.log("enter name to create blueprint");
			driver.findElement(By.xpath("//button[@id='newBlueprintPopup_Create_Button']")).click();
			Thread.sleep(3000);
			Reporter.log("click on create to create blueprint");
			driver.findElement(By.xpath("//button[@id='newBlueprintPopup_OpenCreated_Button']")).click();
			Thread.sleep(3000);
			Reporter.log("click on open button");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
		 }
	 @Test(priority =2)
	  public void root_input_section3_fifth ()throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(3000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    driver.findElement(By.xpath("//td[@class='td-bp-name'][normalize-space()='blueprintforinput']")).click();
			Thread.sleep(3000);
			Reporter.log("Click on blueprintforinput blueprint");
			driver.findElement(By.xpath("//button[@class='btn btn-purple'][normalize-space()='Add Inputs']")).click();
			Thread.sleep(3000);
			Reporter.log("Click on add input blueprint");
			driver.findElement(By.xpath("//div[@class='name']//input")).clear();
			Thread.sleep(3000);
			Reporter.log("clear textbox");
			driver.findElement(By.xpath("//div[@class='name']//input")).sendKeys("input name for test");
			Thread.sleep(3000);
			Reporter.log("enter name for input");
			driver.findElement(By.xpath("(//i[@class='icon yaml-editor-plus-purple'])[3]")).click();
			Thread.sleep(3000);
			Reporter.log("Click on section 2");
			driver.findElement(By.xpath("(//i[@class='icon sidebar-checkbox-light-blue-unchecked'])[7]")).click();
			Thread.sleep(3000);
			Reporter.log("Click on seventh radio button");
			driver.findElement(By.xpath("//div[@class='ng-input']//input")).click();
			Thread.sleep(3000);
			Reporter.log("Click on drop down");
			driver.findElement(By.xpath("(//span[@class='ng-option-label'])[1]")).click();
			Thread.sleep(3000);
			Reporter.log("Click on first");
			driver.findElement(By.xpath("//div[@class='func-header']//input")).sendKeys("enter parameter of api");
			Thread.sleep(3000);
			Reporter.log("enter into parameter");
			driver.findElement(By.xpath("//div[@class='func-header']//i")).click();
			Thread.sleep(3000);
			Reporter.log("click on add button");
			driver.findElement(By.xpath("//div[@class='param']//i[@class='icon constraint-setting-white']")).click();
			Thread.sleep(3000);
			Reporter.log("click on settings button");
			driver.findElement(By.xpath("(//div[@class='ng-input']//input)[2]")).click();
			Thread.sleep(3000);
			Reporter.log("click on parammeter dropdown");
			driver.findElement(By.xpath("(//span[@class='ng-option-label'])[1]")).click();
			Thread.sleep(3000);
			Reporter.log("select option from drop down");
			driver.findElement(By.xpath("(//span[@class='ng-arrow-wrapper'])[3]")).click();
			Thread.sleep(3000);
			Reporter.log("click on drop down");
			driver.findElement(By.xpath("//span[@class='ng-option-label']")).click();
			Thread.sleep(3000);
			Reporter.log("select option from drop down");
			driver.findElement(By.xpath("//i[@class='icon constraint-x-circle-red']")).click();
			Thread.sleep(3000);
			Reporter.log("close the dailog box");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
			
			
			
			
			
			
	  }
	 @Test(priority =3)
	  public void deleting_of_blueprintforinput () throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(3000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.xpath("//td[normalize-space()='blueprintforinput']/following-sibling::td/span[@class='table-btn-delete']")).click();
			Thread.sleep(3000);
			Reporter.log("deleting blueprintforinput");
			driver.findElement(By.xpath("//button[@class='btn action-btn']")).click();
			Thread.sleep(3000);
			Reporter.log("click on delete option");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn']")).click();
			Thread.sleep(3000);
			Reporter.log("click on okay option");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
	 }
}
