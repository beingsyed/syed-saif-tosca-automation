package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class CanceltheBpfromADVSearch {
  @Test
  public void CanceltheBpfromADVSearchPopup() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Thread.sleep(1550);
		Reporter.log("Browser Session Maximized");
		driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
	    System.out.println(driver.getTitle());
	    Thread.sleep(1550);
		Reporter.log("Navigate to BluePrint Editor");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("blue");
	    Thread.sleep(1550);
		Reporter.log("Enter the Text in SERACH TEXTBOX");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.xpath("//i[@class='fas fa-search']")).click();
	    Thread.sleep(1550);
		Reporter.log("CLICK ON SERACH TEXTBOX");
	    driver.findElement(By.xpath("//button[@id='advSerch_Popup_maxBtn']")).click();
	    Thread.sleep(1550);
		Reporter.log("CLICK ON MAXIMIZE BUTTON");
	    System.out.println(driver.getTitle());
	    Thread.sleep(1550);
	    WebElement element5 = driver.findElement(By.xpath(BpConstants.SELECT_AUTHOR1_DROPDOWN));
		Actions actions5 = new Actions(driver);
		actions5.moveToElement(element5).click().build().perform();
	    Thread.sleep(1550);
		Reporter.log("select the Authorfrom Dropdown");
		driver.findElement(By.name(BpConstants.SELECT_FROM_DATE)).sendKeys("01/01/2019");
		Thread.sleep(1550);
		Reporter.log("select the FROM DATE");
		driver.findElement(By.name(BpConstants.SELECT_TO_DATE)).sendKeys("17/12/2019");
		Thread.sleep(1550);
		Reporter.log("select the TO DATE");
		WebElement element = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();
		Reporter.log("CLICK ON TOGGLE LINK0");
		driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0)).click();
		Thread.sleep(1550);
	    System.out.println(driver.getTitle());
		Reporter.log("CLICK ON TOGGLE LINK0");
		WebElement element1 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK1));
		Actions actions1 = new Actions(driver);
		actions1.moveToElement(element1).click().build().perform();
		Thread.sleep(1550);
		Reporter.log("CLICK ON TOGGLE LINK1");
	    System.out.println(driver.getTitle());
		WebElement element2 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK2));
		Actions actions2 = new Actions(driver);
		actions2.moveToElement(element2).click().build().perform();
		Thread.sleep(1550);
		Reporter.log("CLICK ON TOGGLE LINK2");
		WebElement element3 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK3));
		Actions actions3 = new Actions(driver);
		actions3.moveToElement(element3).click().build().perform();
		Thread.sleep(1550);
		Reporter.log("CLICK ON TOGGLE LINK3");
		WebElement element4 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
		Actions actions4 = new Actions(driver);
		actions4.moveToElement(element4).click().build().perform();
		Thread.sleep(1550);
		Reporter.log("CLICK ON TOGGLE LINK4");
		driver.findElement(By.xpath("//button[@id='advSerch_Filter_ApplyBtn']")).click();
		Thread.sleep(1550);
		Reporter.log("CLICK ON Apply BUTTON ");
		driver.findElement(By.xpath("//tr[@class='ng-star-inserted'][1]/td[1]/i")).click();
		Thread.sleep(1550);
		Reporter.log("Select Check box from Advance search window");
		driver.findElement(By.xpath("//tr[@class='ng-star-inserted'][1]/td[9]/span[@class='table-btn-delete']")).click();
		Thread.sleep(1550);
		Reporter.log("click on delete button Advance search window");
	    driver.findElement(By.xpath("//button[@class='btn cancel-btn']")).click();
		Thread.sleep(1550);
		Reporter.log("Click on Cancel button in ADV Search window");
		Thread.sleep(1550);
		driver.findElement(By.xpath("//button[@class='custom-btn btn-secondary']")).click();
		Reporter.log("Click on CANCEL Button");
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();
	  
  }
 

}
