package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class Searchoperationunderopenblueprintbyprovidingavalidinput {
  @Test
  public void OpentheSelectedBlueprintwithvalidInput() throws Exception 
  {
	  
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Reporter.log("Browser Session maximized");
	    try {
			driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN1_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on New Buttom from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_SEARCH1_BLUEPRINTS)).sendKeys("test");
			Thread.sleep(2550);
			Reporter.log("Enter the BPname from Slide Bar");
			driver.findElement(By.xpath(BpConstants.SLIDEBAR_CLICK1_BPRECORD1)).click();
			Thread.sleep(2550);
			Reporter.log("Click on SELECTED BPRECORD1");
			driver.findElement(By.xpath(BpConstants.SLIDEBAR_CLICK2_BPRECORD2)).click();
			Thread.sleep(2550);
			Reporter.log("Click on SELECTED BPRECORD2");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//button[normalize-space()='Open']")).click();
		Thread.sleep(2550); 
		Reporter.log("Click on Open Button SLIDEBAR");
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();
		
	  
  }
 

}
