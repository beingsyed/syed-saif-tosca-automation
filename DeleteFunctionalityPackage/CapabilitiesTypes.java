package DeleteFunctionalityPackage;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class CapabilitiesTypes {

	@Test
	public void Clone() throws Exception {
		
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div/div[3]/div[1]")).click();
		Thread.sleep(2500);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[1]/div[1]/a[1]")).click();
		Thread.sleep(2500);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[2]/div/ol/li[5]/div/div[1]/a[2]/i")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[2]/div/ol/li[5]/div/div[2]/i")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[1]/div[2]/input")).clear();
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[1]/div[2]/input")).sendKeys("New Capabilities");
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[2]/div[1]/div[2]/ng-select")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[2]/div[1]/div[2]/ng-select/ng-dropdown-panel/div/div[2]/div[4]/span")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[2]/div[2]/div[2]/app-occurrences/div/a/div[1]/div/div[2]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[2]/div[2]/div[2]/app-occurrences/div/div[2]/a[2]/i")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[2]/div[3]/div[2]/textarea")).sendKeys("Lorem Ipsum");
		Thread.sleep(2000);
		driver.quit();
	}

}
