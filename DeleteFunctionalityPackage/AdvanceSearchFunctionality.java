package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class AdvanceSearchFunctionality 
{
	@Test(priority = 0)

	  public void AdvancedsearchfromDashboard() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("IE Browser Maximized ");
			Thread.sleep(1550);
			driver.findElement(By.id(BpConstants.ENTER_TEXT_SEARCH_TEXTBOX)).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the text");
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
			Thread.sleep(1050);
			Reporter.log("Click on SEARCH Button");
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
		    driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
			Thread.sleep(1550);
			Reporter.log("Click on Maximize Button");
			driver.findElement(By.name(BpConstants.SELECT_FROM_DATE)).sendKeys("01/01/2019");
			Thread.sleep(1550);
			Reporter.log("Enter the FROMDATE");
			driver.findElement(By.name(BpConstants.SELECT_TO_DATE)).sendKeys("17/12/2019");
			Thread.sleep(1550);
			Reporter.log("Enter the TODATE");		   
			Thread.sleep(1550);
			WebElement element = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().build().perform();
			Reporter.log("CLICK ON TOGGLE LINK0");
			driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0)).click();
			Thread.sleep(1550);
		    System.out.println(driver.getTitle());
			Reporter.log("CLICK ON TOGGLE LINK0");
			WebElement element1 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK1));
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(element1).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK1");
		    System.out.println(driver.getTitle());
			WebElement element2 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK2));
			Actions actions2 = new Actions(driver);
			actions2.moveToElement(element2).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK2");
			WebElement element3 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK3));
			Actions actions3 = new Actions(driver);
			actions3.moveToElement(element3).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK3");
			WebElement element4 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions4 = new Actions(driver);
			actions4.moveToElement(element4).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK4");
			Thread.sleep(1550);
			WebElement element5 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions5 = new Actions(driver);
			actions5.moveToElement(element5).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK5");
			driver.findElement(By.id(BpConstants.CLICK_ON_APPLY_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Apply Button");
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
		    driver.findElement(By.xpath(BpConstants.SELECT_ALL_RECORDS)).click();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("SELECT ALL THE RECORDS");
			driver.findElement(By.id("advSerch_Action_OpenBtn")).click();
			Thread.sleep(1550);
			Reporter.log("Open Selected BP Records");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
  
 
}
	 @Test(priority =1)
	  public void SearchTextBoxDashboard() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1000);
			Reporter.log("Browser is Launched in Internet Explorer");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session Maximized");
			Thread.sleep(1000);
			Reporter.log("Browser is maximized in Internet Explorer");
			driver.findElement(By.id(BpConstants.ENTER_TEXT_SEARCH_TEXTBOX)).sendKeys("test");
			Thread.sleep(1000);
			Reporter.log("Enter the Text Search TextBox in Internet Explorer");		
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
	        Thread.sleep(1000);
			Reporter.log("Click on Search TextBox in Internet Explorer");
			Thread.sleep(1550);
			Reporter.log("CLICK ON SERACH TEXTBOX");
		    driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);		   
			driver.findElement(By.name(BpConstants.SELECT_FROM_DATE)).sendKeys("01/01/2019");
			Thread.sleep(1550);
			Reporter.log("select the FROM DATE");
			driver.findElement(By.name(BpConstants.SELECT_TO_DATE)).sendKeys("17/12/2019");
			Thread.sleep(1550);
			Reporter.log("select the TO DATE");
			WebElement element = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().build().perform();
			Reporter.log("CLICK ON TOGGLE LINK0");
			driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0)).click();
			Thread.sleep(1550);
		    System.out.println(driver.getTitle());
			Reporter.log("CLICK ON TOGGLE LINK0");
			WebElement element1 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK1));
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(element1).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK1");
		    System.out.println(driver.getTitle());
			WebElement element2 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK2));
			Actions actions2 = new Actions(driver);
			actions2.moveToElement(element2).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK2");
			WebElement element3 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK3));
			Actions actions3 = new Actions(driver);
			actions3.moveToElement(element3).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK3");
			WebElement element4 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions4 = new Actions(driver);
			actions4.moveToElement(element4).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK4");
			Thread.sleep(1550);
			WebElement element5 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions5 = new Actions(driver);
			actions5.moveToElement(element5).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK5");
			driver.findElement(By.id(BpConstants.CLICK_ON_APPLY_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON APPLY BUTTON ");
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();



	  }
	 @Test(priority =2)
	  public void AdvanceSearchwithValidCharacters() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1000);
			Reporter.log("Browser is Launched in Internet Explorer");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session Maximized");
			Thread.sleep(1000);
			Reporter.log("Browser is maximized in Internet Explorer");
			driver.findElement(By.id(BpConstants.ENTER_TEXT_SEARCH_TEXTBOX)).sendKeys("Blue");
			Thread.sleep(1000);
			Reporter.log("Enter the Text Search TextBox in Internet Explorer");		
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
	        Thread.sleep(1000);
			Reporter.log("Click on Search TextBox in Internet Explorer");
			Thread.sleep(1550);
			Reporter.log("CLICK ON SERACH TEXTBOX");
			driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);		    
			driver.findElement(By.name(BpConstants.SELECT_FROM_DATE)).sendKeys("01/01/2019");
			Thread.sleep(1550);
			Reporter.log("select the FROM DATE");
			driver.findElement(By.name(BpConstants.SELECT_TO_DATE)).sendKeys("17/12/2019");
			Thread.sleep(1550);
			Reporter.log("select the TO DATE");
			WebElement element = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().build().perform();
			Reporter.log("CLICK ON TOGGLE LINK0");
			driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0)).click();
			Thread.sleep(1550);
		    System.out.println(driver.getTitle());
			Reporter.log("CLICK ON TOGGLE LINK0");
			WebElement element1 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK1));
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(element1).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK1");
		    System.out.println(driver.getTitle());
			WebElement element2 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK2));
			Actions actions2 = new Actions(driver);
			actions2.moveToElement(element2).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK2");
			WebElement element3 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK3));
			Actions actions3 = new Actions(driver);
			actions3.moveToElement(element3).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK3");
			WebElement element4 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions4 = new Actions(driver);
			actions4.moveToElement(element4).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK4");
			Thread.sleep(1550);
			WebElement element5 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions5 = new Actions(driver);
			actions5.moveToElement(element5).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK5");
			Thread.sleep(1550);
			driver.findElement(By.id(BpConstants.CLICK_ON_APPLY_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON APPLY BUTTON ");
		    driver.findElement(By.xpath(BpConstants.SELECT_ALL_RECORDS)).click();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("SELECT ALL THE RECORDS");
			Thread.sleep(1550);
			Reporter.log("Select Check box from Advance search window");
			driver.findElement(By.id(BpConstants.CLICK_OPEN_ADVSEARCH)).click();
			Thread.sleep(1550);
			Reporter.log("Select Open button from Advance search window");
			Thread.sleep(1550);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority =3)
	  public void SERACHFUNCIONALITYWITHAPLPHABETICALS() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Session Maximized");
			driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
			Reporter.log("Navigate to BluePrint Editor");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("QWERTYqwerty");
		    Thread.sleep(1550);
			Reporter.log("Enter the Text in SERACH TEXTBOX");
		    System.out.println(driver.getTitle());
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON SERACH TEXTBOX");
		    driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_ON_CLEAR_BUTTON)).click();
		    Reporter.log("CLICK ON CLEAR BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550); 
		    driver.findElement(By.id(BpConstants.CLICK_CANCEL_ADVSEARCH)).click();
		    Reporter.log("CLICK ON CANCELADV BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority =4)
	  public void BlueprintEditorSEARCHFunctionality() throws Exception 
	  {
		  
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Session Maximized");
			driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
			Reporter.log("Navigate to BluePrint Editor");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("test");
		    Thread.sleep(1550);
			Reporter.log("Enter the Text in SERACH TEXTBOX");
		    System.out.println(driver.getTitle());
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON SERACH TEXTBOX");
		    driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
			driver.findElement(By.name(BpConstants.SELECT_FROM_DATE)).sendKeys("01/01/2019");
			Thread.sleep(1550);
			Reporter.log("select the FROM DATE");
			driver.findElement(By.name(BpConstants.SELECT_TO_DATE)).sendKeys("17/12/2019");
			Thread.sleep(1550);
			Reporter.log("select the TO DATE");
			WebElement element = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().build().perform();
			Reporter.log("CLICK ON TOGGLE LINK0");
			driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0)).click();
			Thread.sleep(1550);
		    System.out.println(driver.getTitle());
			Reporter.log("CLICK ON TOGGLE LINK0");
			WebElement element1 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK1));
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(element1).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK1");
		    System.out.println(driver.getTitle());
			WebElement element2 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK2));
			Actions actions2 = new Actions(driver);
			actions2.moveToElement(element2).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK2");
			WebElement element3 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK3));
			Actions actions3 = new Actions(driver);
			actions3.moveToElement(element3).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK3");
			WebElement element4 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions4 = new Actions(driver);
			actions4.moveToElement(element4).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK4");			
			driver.findElement(By.id(BpConstants.CLICK_ON_APPLY_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON APPLY BUTTON ");
		    driver.findElement(By.xpath(BpConstants.SELECT_ALL_RECORDS)).click();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("SELECT ALL THE RECORDS");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
			Reporter.log("Select Check box from Advance search window");
			driver.findElement(By.id(BpConstants.CLICK_OPEN_ADVSEARCH)).click();
			Thread.sleep(1550);
			Reporter.log("Select Open button from Advance search window");
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority =5)
	  public void Wildcardsearcfunctionlity() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Session Maximized");
			driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
			Reporter.log("Navigate to BluePrint Editor");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("ABDCEFGH");
		    Thread.sleep(1550);
			Reporter.log("Enter the Text in SERACH TEXTBOX");
		    System.out.println(driver.getTitle());
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON SERACH TEXTBOX");
		    driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_ON_CLEAR_BUTTON)).click();
		    Reporter.log("CLICK ON CLEAR BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_ON_CANCEL_BUTTON)).click();
		    Reporter.log("CLICK ON CANCELADV BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
		  
	  }
	 @Test(priority =6)
	  public void SearchwithValues() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Session Maximized");	
				driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
				System.out.println(driver.getTitle());
				Thread.sleep(1550);
				Reporter.log("Navigate to BluePrint Editor");
				System.out.println(driver.getTitle());
				driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("1234ABCDE");
				Thread.sleep(1550);
				Reporter.log("Enter the Text in SERACH TEXTBOX");
				System.out.println(driver.getTitle());
				driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
				Thread.sleep(1550);
				Reporter.log("CLICK ON SERACH TEXTBOX");
				driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
				Thread.sleep(10550);
			    Reporter.log("CLICK ON MAXIMIZE BUTTON");
		        System.out.println(driver.getTitle());
		        Thread.sleep(1550);
		        driver.findElement(By.id("advSerch_Filter_ClearBtn")).click();
		    Reporter.log("CLICK ON CLEAR BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_ONCANCELADV_BUTTON)).click();
		    Reporter.log("CLICK ON CANCELADV BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
	  }
	 @Test(priority =7)
	  public void ValidSearch() throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1000);
			Reporter.log("Browser is Launched in Internet Explorer");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session Maximized");
			Thread.sleep(1000);
			Reporter.log("Browser is maximized in Internet Explorer");
			driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("Lorem Ipsum Dolor Sit Amet Test Lorem Ip");
			Thread.sleep(1000);
			Reporter.log("Enter the Text Search TextBox in Internet Explorer");		
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
	        Thread.sleep(1000);
			Reporter.log("Click on Search TextBox in Internet Explorer");
			Thread.sleep(1550);
			Reporter.log("CLICK ON SERACH TEXTBOX");
			driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);		  
			driver.findElement(By.name(BpConstants.SELECT_FROM_DATE)).sendKeys("01/01/2019");
			Thread.sleep(1550);
			Reporter.log("select the FROM DATE");
			driver.findElement(By.name(BpConstants.SELECT_TO_DATE)).sendKeys("16/12/2019");
			Thread.sleep(1550);
			Reporter.log("select the TO DATE");
			WebElement element = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().build().perform();
			Reporter.log("CLICK ON TOGGLE LINK0");
			driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK0)).click();
			Thread.sleep(1550);
		    System.out.println(driver.getTitle());
			Reporter.log("CLICK ON TOGGLE LINK0");
			WebElement element1 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK1));
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(element1).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK1");
		    System.out.println(driver.getTitle());
			WebElement element2 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK2));
			Actions actions2 = new Actions(driver);
			actions2.moveToElement(element2).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK2");
			WebElement element3 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK3));
			Actions actions3 = new Actions(driver);
			actions3.moveToElement(element3).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK3");
			WebElement element4 = driver.findElement(By.id(BpConstants.CLICK_ON_TOGGLE_LINK4));
			Actions actions4 = new Actions(driver);
			actions4.moveToElement(element4).click().build().perform();
			Thread.sleep(1550);
			Reporter.log("CLICK ON TOGGLE LINK4");
			driver.findElement(By.id(BpConstants.CLICK_ON_APPLY_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON APPLY BUTTON ");
			driver.findElement(By.xpath(BpConstants.SELECT_ALL_RECORDS)).click();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("SELECT ALL THE RECORDS");
		    System.out.println(driver.getTitle());
		    Reporter.log("Select Check box from Advance search window");
		    driver.findElement(By.id(BpConstants.CLICK_OPEN_ADVSEARCH)).click();
			Thread.sleep(1550);
			Reporter.log("Select Open button from Advance search window");
			Thread.sleep(1550);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit();
		  
	  }
	 @Test(priority =8)
	  public void SEARCHWITHNUMERICANDALPHBETS() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Browser Session Maximized");
			driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
			Reporter.log("Navigate to BluePrint Editor");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("QWER12345qwer");
		    Thread.sleep(1550);
			Reporter.log("Enter the Text in SERACH TEXTBOX");
		    System.out.println(driver.getTitle());
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON SEARCH TEXTBOX");
			driver.findElement(By.className("header-action-btn")).click();
		    Thread.sleep(1550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_ON_CLEAR_BUTTON)).click();
		    Reporter.log("CLICK ON CLEAR BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_CANCEL_ADVSEARCH)).click();
		    Reporter.log("CLICK ON CANCELADV BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit(); 
	  }  
	 @Test(priority =9)
	  public void SearchoperationunderopenblueprintInvalidinputDashboard() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN1_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on New Buttom from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_SEARCH1_BLUEPRINTS)).sendKeys("Moorthy");
			Thread.sleep(1550);
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();

			
			
		    
	  }
	 @Test(priority =10)
	  public void OpentheSelectedBlueprintwithvalidInput() throws Exception 
	  {
		  
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
		  
				driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN1_BUTTON)).click();
				Thread.sleep(1550);
				Reporter.log("Click on New Buttom from SLIDEBAR");
				driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_SEARCH1_BLUEPRINTS)).sendKeys("test");
				Thread.sleep(2550);
				Reporter.log("Enter the BPname from Slide Bar");
				
				driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
				Thread.sleep(1550);
				Reporter.log("Click on SELECTED BPRECORD1");
				driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/span")).click();
				Thread.sleep(1550);
				Reporter.log("Click on SELECTED BPRECORD2");
			    driver.findElement(By.xpath("//*[@id=\"sidenav_open_openBtn\"]")).click();
				Thread.sleep(1550); 
				Reporter.log("Click on Open Button SLIDEBAR");
				driver.close();
				Reporter.log("Browser Session Closed");
				driver.quit();
			
		  
	  }
	 @Test(priority =11)	 
	  public void AdvanceSearchwithNumericals() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Thread.sleep(1050);
			Reporter.log("Browser Session Maximized");	
			driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Template Editor']")).click();
			System.out.println(driver.getTitle());
			Thread.sleep(1550);
			Reporter.log("Navigate to BluePrint Editor");
			System.out.println(driver.getTitle());
			driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("1231");
			Thread.sleep(1550);
			Reporter.log("Enter the Text in SERACH TEXTBOX");
			System.out.println(driver.getTitle());
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i")).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON SEARCH TEXTBOX");
			driver.findElement(By.xpath("//i[@class='fas fa-expand']")).click();
			Thread.sleep(10550);
			Reporter.log("CLICK ON MAXIMIZE BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_ON_CLEAR_BUTTON)).click();
		    Reporter.log("CLICK ON CLEAR BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.findElement(By.id(BpConstants.CLICK_CANCEL_ADVSEARCH)).click();
		    Reporter.log("CLICK ON CANCELADV BUTTON");
		    System.out.println(driver.getTitle());
		    Thread.sleep(1550);
		    driver.close();
			Reporter.log("Browser Session Closed");
		    driver.quit(); 
	  }
			
 
}