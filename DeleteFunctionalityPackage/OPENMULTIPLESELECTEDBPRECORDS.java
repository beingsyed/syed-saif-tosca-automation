package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class OPENMULTIPLESELECTEDBPRECORDS {
  @Test
  public void MultiplerecordsSelected() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Reporter.log("Browser Session maximized");
	    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN1_BUTTON)).click();
		Thread.sleep(1550);
		Reporter.log("Click on New Buttom from SLIDEBAR");
		driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
		Thread.sleep(1550);
		Reporter.log("Enter the BPname from Slide Bar");
		driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
		Thread.sleep(1550);
		Reporter.log("Click on SELECTED BPRECORD1");
		driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/span")).click();
		Thread.sleep(1550);
		Reporter.log("Click on SELECTED BPRECORD2");
	    driver.findElement(By.xpath("//*[@id=\"sidenav_open_openBtn\"]")).click();
		Thread.sleep(1550); 
		Reporter.log("Click on Open Button SLIDEBAR");
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();
		
  }
 

}
