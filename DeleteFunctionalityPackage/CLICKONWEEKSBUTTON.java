package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class CLICKONWEEKSBUTTON {
  @Test
  public void ClickonWeeksButtonfromDashboard() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
      System.out.println(driver.getTitle());
		Reporter.log("Browser Session maximized");
		Thread.sleep(1550);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div/div/app-dashboard/div/div[1]/div[2]/button[1]")).click();
		Thread.sleep(1550);
		driver.findElement(By.xpath("/html/body/app-root/div/div/div/div/app-dashboard/div/div[1]/div[2]/button[2]")).click();
		Thread.sleep(1550);
		 driver.close();
		    driver.quit();
  }
 

}
