package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SortBluePrintRecordsFunctionality 
{
	@Test(priority =0)
	  public void SORTRECORDSBYNAME() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
			Reporter.log("IE Browser Maximized ");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON UPARROW BUTTON");
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME )).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON DOWNARROW BUTTON");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();



	  }
	 @Test(priority =1)

	  public void SORTRECORDSByCREATEDDATE() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
			Reporter.log("Browser Session maximized");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME2)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON UPARROW BUTTON");
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME2 )).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON DOWNARROW BUTTON");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit(); 
	  }
	 @Test(priority =2)
	  public void SORTRECORDSByLASTMODIFIEDDATE() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME4)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON UPARROW BUTTON");
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME4)).click();
			Reporter.log("CLICK ON DOWNARROW BUTTON");
			Thread.sleep(1550);
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit(); 
	  }
	 @Test(priority =3)
	  public void SORTRECORDSBYSTATUSDASH() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
			Reporter.log("Browser Session Maximized");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME5)).click();
			Thread.sleep(2550);
			Reporter.log("Click on Uparrow Button");
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME5)).click();
			Thread.sleep(1550);
			Reporter.log("Click on Downarrow Button");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit(); 
		  
	  }
	 @Test(priority = 4)
	  public void CREATEDBY_SORT_RECORDS() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
			Reporter.log("Browser Session maximized");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME1)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON UPARROW BUTTON");
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME1 )).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON DOWNARROW BUTTON");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
		  
	  }
	 @Test(priority =5)
	  public void SorttheRecordsLastUpdatedBy() throws Exception
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
			Reporter.log("Browser Session maximized");
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME3)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON UPARROW BUTTON");
		    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME3)).click();
			Thread.sleep(1550);
			Reporter.log("CLICK ON DOWNARROW BUTTON");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit(); 
		  
	  }
 
}