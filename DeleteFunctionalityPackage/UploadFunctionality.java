package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class UploadFunctionality 
{
  
	
	 @Test(priority =0)
	  public void ValidFile() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			Thread.sleep(1550);
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on New Button from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
			Thread.sleep(1550);
			Reporter.log("Click on Browse  Button from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[4]/button[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on UploadButton from SLIDEBAR");
			Thread.sleep(1550);
			driver.close();
			driver.quit();
	  }
	  
	  @Test(priority = 1)
	  public void UploadYAMLFILEFROMSLIDEBAR() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1500);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			Thread.sleep(1550);
	        JavascriptExecutor js = (JavascriptExecutor)driver;		
	        js.executeScript("window.scrollBy(0,6000)");			
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on New Button from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
			Thread.sleep(1550);
			Reporter.log("Click on Browse  Button from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[4]/button[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on UploadButton from SLIDEBAR");
			Thread.sleep(1550);
			driver.close();
			driver.quit();
			
			
	  }
	  
	  @Test(priority =2)
	  public void ImportInvalidFile() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			Thread.sleep(1550);
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(2550);
			Reporter.log("Click on New Button from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
			Thread.sleep(1550);
			Reporter.log("Click on Browse  Button from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[4]/button[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on UploadButton from SLIDEBAR");
			Thread.sleep(1550);
			driver.close();
			driver.quit();
		  
	  }

	  @Test(priority = 3)
	  public void  BlueprintfilewithblankdatainitscontentviadraganddropFROMSLIDEBAR() throws Exception
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			Thread.sleep(1550);
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on New Button from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
			Thread.sleep(1550);
			Reporter.log("Click on Browse  Button from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[4]/button[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on UploadButton from SLIDEBAR");
			Thread.sleep(1550);
			driver.close();
			driver.quit();
		  
	  }
	  
	  @Test(priority = 4)
	  public void  EdittheYamlFile() throws Exception
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			Thread.sleep(1550);
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on New Button from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
			Thread.sleep(1550);
			Reporter.log("Click on Browse  Button from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[4]/button[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on UploadButton from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[1]/button[1]/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on Edit Button from SLIDEBAR");
			Thread.sleep(1550);
			driver.close();			
			driver.quit();
		  
	  }
	  
	  
	  @Test(priority = 5)
	  public void  DeletetheYamlFile() throws Exception
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Reporter.log("Browser Session Started");
			Thread.sleep(1550);
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
			Reporter.log("Browser Session maximized");
			Thread.sleep(1550);
		    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on New Button from SLIDEBAR");
			driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
			Thread.sleep(1550);
			Reporter.log("Click on Browse  Button from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[4]/button[1]")).click();
			Thread.sleep(1550);
			Reporter.log("Click on UploadButton from SLIDEBAR");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/app-yaml-upload/div/div/div[1]/button[2]/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on delete Button from SLIDEBAR");
			Thread.sleep(1550);
			driver.close();
			driver.quit();
		  
	  } 
	  
      
  
	 

	  
	  
}