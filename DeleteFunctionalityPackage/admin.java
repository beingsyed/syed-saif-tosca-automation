package DeleteFunctionalityPackage;
import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class admin{
	WebDriver driver;
	@Test()
	public void adminTools() {
		try {
			 FileInputStream fi = new FileInputStream("config.properties");
				Properties prop= new Properties();
				prop.load(fi);
				System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
				WebDriver driver = new InternetExplorerDriver();
				driver.get(prop.getProperty("baseURL"));
				Thread.sleep(1550);
				Reporter.log("Browser Session Started");
			    driver.manage().window().maximize();
				Reporter.log("Browser Session Maximized");
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div/div[3]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[1]/div[1]/a[1]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[2]/div/ol/li[7]/div/div[1]/a[2]")).click();
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[2]/div/ol/li[7]/div/div[2]/i")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[1]/div[2]/input")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[1]/div[2]/input")).sendKeys("Test");
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[2]/div[2]/ng-select/div/div/div[2]/input")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[2]/div[2]/ng-select/ng-dropdown-panel/div/div[2]/div[2]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[3]/div[1]/a")).click();
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[3]/div[2]/div[3]/input")).clear();
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[3]/div[2]/div[3]/input")).sendKeys(" New test");
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[3]/div[2]/div[3]/div/i[1]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[4]/div[1]/a")).click();
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[4]/div[2]/div/div[1]/input")).clear();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[4]/div[2]/div/div[1]/input")).sendKeys(" New test check");
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[4]/div[2]/div/div[2]/ng-select/div/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[4]/div[2]/div/div[2]/ng-select/ng-dropdown-panel/div/div[2]/div[3]/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[4]/div[2]/div/div[3]/ng-select/div/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/app-root/div/div/div[2]/div/app-admin/div/div/div[2]/div[1]/app-types-editor-page/div/div/div[4]/div[2]/div/div[3]/ng-select/ng-dropdown-panel/div/div[2]/div[3]/span")).click();
			Thread.sleep(2000);
			Reporter.log("Browser Session Closed");
		    driver.quit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}