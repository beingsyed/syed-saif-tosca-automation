package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ClickonCancelButtonfromCloneBp {
  @Test
  public void ClickonCancelButtonfromCloneBpdashboard() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1000);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Thread.sleep(1000);
		Reporter.log("Browser Session Maximized");
	    System.out.println(driver.getTitle());
		Reporter.log("Browser Maximized");
		driver.findElement(By.xpath("//td[normalize-space()='blue24']/following-sibling::td/span[@class='table-btn-copy']")).click();
		Thread.sleep(1000);
		Reporter.log("Click on Clone Button");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.id(BpConstants.CLICK_ON_CANCEL_BUTTON_DASHBOARD_POPUP2)).click();
	    Thread.sleep(1000);
		Reporter.log("Click on Cancel");
	    System.out.println(driver.getTitle());
	    driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();
	  
  }
 

}
