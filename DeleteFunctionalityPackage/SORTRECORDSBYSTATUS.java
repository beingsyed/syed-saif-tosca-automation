package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SORTRECORDSBYSTATUS {
  @Test
  public void SORTRECORDSBYSTATUSDASH() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Reporter.log("Browser Session Started");
		Thread.sleep(1550);
	    driver.manage().window().maximize();
		Reporter.log("Browser Session Maximized");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME5)).click();
		Thread.sleep(2550);
		Reporter.log("Click on Uparrow Button");
	    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME5)).click();
		Thread.sleep(1550);
		Reporter.log("Click on Downarrow Button");
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit(); 
	  
  }
 

}
