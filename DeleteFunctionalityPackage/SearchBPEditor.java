package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SearchBPEditor {
  @Test
  public void SEARCHWITHNUMERICANDALPHBETS() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1000);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Thread.sleep(1000);
		Reporter.log("Browser Session Maximized");
		driver.findElement(By.xpath("(//div[@class='item'])[4]")).click();
	    System.out.println(driver.getTitle());
	    Thread.sleep(1000);
		Reporter.log("Navigate to BluePrint Editor");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.id(BpConstants.SEARCH_TEXT_FIELD)).sendKeys("blue");
	    Thread.sleep(1000);
		Reporter.log("Enter the Text in SERACH TEXTBOX");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.xpath("//i[@class='fas fa-search']")).click();
	    Thread.sleep(1000);
		Reporter.log("CLICK ON SEARCH TEXTBOX");
		driver.findElement(By.className("header-action-btn")).click();
	    Thread.sleep(1000);
		Reporter.log("CLICK ON MAXIMIZE BUTTON");
	    System.out.println(driver.getTitle());
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id=\"advSerch_Filter_ClearBtn\"]")).click();
	    Reporter.log("CLICK ON CLEAR BUTTON");
	    System.out.println(driver.getTitle());
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id=\"advSerch_Action_CancelBtn\"]" )).click();
	    //driver.findElement(By.xpath("/html/body/app-root/div/div/app-search-blueprint/div/div[2]/div[2]/div[2]/button[1]")).click();
	    Reporter.log("CLICK ON CANCELADV BUTTON");
	    System.out.println(driver.getTitle());
	    Thread.sleep(1000);
	    driver.close();
		Reporter.log("Browser Session Closed");
	    driver.quit(); 
  }
 

}
