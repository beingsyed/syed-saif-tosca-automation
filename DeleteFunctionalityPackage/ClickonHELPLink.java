package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ClickonHELPLink {
  @Test
  public void CLICKONHELPLINK() throws Exception 
  {

	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1000);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
        System.out.println(driver.getTitle());
		Thread.sleep(1000);
		Reporter.log("Browser Session Maximized");
	    driver.findElement(By.xpath(BpConstants.SLIDEBAR_HELP_LINK)).click();
		Thread.sleep(1000);
		Reporter.log("Click on Help Link");
		driver.findElement(By.className(BpConstants.SLIDEBAR_HELPLINK_CLOSE)).click();
		Thread.sleep(1000);
		Reporter.log("Click on Help Close Button");
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();


		

	  
  }
  

}
