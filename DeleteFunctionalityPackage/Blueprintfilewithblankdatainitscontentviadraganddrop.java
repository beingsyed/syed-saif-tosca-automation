package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class   Blueprintfilewithblankdatainitscontentviadraganddrop 
{
  @Test
  public void  BlueprintfilewithblankdatainitscontentviadraganddropFROMSLIDEBAR() throws Exception
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Reporter.log("Browser Session Started");
		Thread.sleep(1550);
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Reporter.log("Browser Session maximized");
		Thread.sleep(1550);
	    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
		Thread.sleep(1550);
		Reporter.log("Click on New Button from SLIDEBAR");
		driver.findElement(By.cssSelector(BpConstants.CLICK_ON_BROWSE_BUTTON)).sendKeys("C:\\Users\\612828917\\Downloads\\sample.yaml");
		Thread.sleep(1550);
		Reporter.log("Click on Browse  Button from SLIDEBAR");
		driver.findElement(By.xpath("//button[@class='custom-btn btn-primary'][normalize-space()='Upload']")).click();
		Thread.sleep(1550);
		Reporter.log("Click on UploadButton from SLIDEBAR");
		driver.close();
		driver.quit();
	  
  }
 
}
