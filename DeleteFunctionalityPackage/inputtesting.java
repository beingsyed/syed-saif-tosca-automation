package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class inputtesting 
{
	 @Test(priority =1)
	  public void create_and_open_blueprint_from_dashboard() throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(3000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    driver.findElement(By.xpath("//i[@class='fas fa-file-medical']")).click();
			Thread.sleep(3000);
			Reporter.log("Click on create blueprint");
			driver.findElement(By.xpath("//input[@id='newBlueprintPopup_newName_Input']")).click();
			Thread.sleep(3000);
			Reporter.log("Click on textbox to create blueprint");
			driver.findElement(By.xpath("//input[@id='newBlueprintPopup_newName_Input']")).sendKeys("blueprintforinput");
			Thread.sleep(3000);
			Reporter.log("enter name to create blueprint");
			driver.findElement(By.xpath("//button[@id='newBlueprintPopup_Create_Button']")).click();
			Thread.sleep(3000);
			Reporter.log("click on create to create blueprint");
			driver.findElement(By.xpath("//button[@id='newBlueprintPopup_OpenCreated_Button']")).click();
			Thread.sleep(3000);
			Reporter.log("click on open button");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
		 }
	 
	 @Test(priority =2)
	  public void root_division_group_cancel() throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(3000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    driver.findElement(By.xpath("//td[@class='td-bp-name'][normalize-space()='blueprintforinput']")).click();
			Thread.sleep(3000);
			Reporter.log("Click on blueprintforinput blueprint");
			driver.findElement(By.xpath("//i[@class='icon']")).click();
			Thread.sleep(3000);
			Reporter.log("open blueprintforinput blueprint");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[normalize-space()='group Name']/../div[3]/input")).sendKeys("Group1");
			Thread.sleep(3000);
			Reporter.log("Entered Group name as Group1");
			driver.findElement(By.xpath("(//i[@class='icon yaml-editor-add-blue'])[1]")).click();
			Thread.sleep(3000);
			Reporter.log("click on add button");
			driver.findElement(By.xpath("(//i[@class='icon yaml-editor-cross-red'])[1]")).click();
			Thread.sleep(3000);
			Reporter.log("click on cancel option");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
	  }
	 @Test(priority =3)
	  public void deleting_of_blueprintforinput () throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(3000);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			driver.findElement(By.xpath("//td[normalize-space()='blueprintforinput']/following-sibling::td/span[@class='table-btn-delete']")).click();
			Thread.sleep(3000);
			Reporter.log("deleting blueprintforinput");
			driver.findElement(By.xpath("//button[@class='btn action-btn']")).click();
			Thread.sleep(3000);
			Reporter.log("click on delete option");
			driver.findElement(By.xpath("//button[@class='btn action-btn success-btn']")).click();
			Thread.sleep(3000);
			Reporter.log("click on okay option");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
	 }
	 
}