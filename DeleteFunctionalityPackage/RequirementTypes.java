package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class RequirementTypes {
	
	@Test
	public void Clone() throws Exception {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		Reporter.log("Clicking on Admin Menu in Sidebar");
		driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div/div[3]/div[1]")).click();
		Thread.sleep(2500);
		Reporter.log("Clicking on Types");
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[1]/div[1]/a[1]")).click();
		Thread.sleep(2500);
		Reporter.log("Clicking on RequirementTypes");
		driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[2]/div/ol/li[6]/div/div[1]/a[2]/i")).click();
		Thread.sleep(3000);
		driver.quit();
	}
	
}
