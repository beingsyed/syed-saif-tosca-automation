package DeleteFunctionalityPackage;
import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class CapabilitiesInAdminMenu {
	
		
		@Test
		public void Clone() throws Exception {
			
			 FileInputStream fi = new FileInputStream("config.properties");
				Properties prop= new Properties();
				prop.load(fi);
				System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
				WebDriver driver = new InternetExplorerDriver();
				driver.get(prop.getProperty("baseURL"));
				Thread.sleep(1550);
				Reporter.log("Browser Session Started");
			    driver.manage().window().maximize();
			    driver.findElement(By.xpath("//div[@class='sidenav-menu-btn-icon sidenav-menu-btn-admin']")).click();
			    Thread.sleep(1000);
			    Reporter.log("click on admin");
			    driver.findElement(By.xpath("(//div[@class='root-node'])[1]")).click();
			    Thread.sleep(1000);
			    Reporter.log("click on types");
			    driver.findElement(By.xpath("/html/body/app-root/div/div/div[1]/div/app-admin-side-bar/div/div[2]/div[2]/div/ol/li[1]")).click();
			    Thread.sleep(1000);
			    Reporter.log("click on capabilites");
			    driver.close();
				Reporter.log("Browser Session Closed");
				driver.quit();
		
	}

}
