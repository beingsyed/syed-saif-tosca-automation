package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class OpenBluePrintsFunctionality 
{
	 @Test(priority =1)
	  public void OPENBLUEPRINTFROMSLIDEBAR() throws Exception 
	  {
		 FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			Reporter.log("Browser Session Maximized");
			System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the BP name from Slidebar");
		    driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on selected Bp Record from Slide bar");
			driver.findElement(By.xpath("//button[@class='custom-btn btn-primary']")).click();
			Thread.sleep(1550); 
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
	  }
	 
	
	@Test(priority = 2)
	  public void MultiplerecordsSelected() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the BP name from Slidebar");
		    driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on selected Bp Record from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on SELECTED BPRECORD2");
		    driver.findElement(By.xpath("//button[@class='custom-btn btn-primary']")).click();
			Thread.sleep(1550); 
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
			
	  }	 

	@Test(priority = 3)
	  public void SingleselectedBPRecord() throws Throwable 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on OPEN Button from Slide bar");
	        driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the BP name from Slidebar");
			 driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
				Thread.sleep(1550);
				Reporter.log("Click on selected Bp Record from Slide bar");
				driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/i")).click();
			Thread.sleep(1550); 
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
	  }	

	 @Test(priority = 4)
	  public void OpenSingleBluePrintRecord() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the BP name from Slidebar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on selected Bp Record from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/i")).click();
			Thread.sleep(1550); 
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();

	  }
	
	 @Test(priority = 5)
	  public void SingleselectedBLueprintRecords() throws Throwable 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the BP name from Slidebar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on selected Bp Record from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/i")).click();
			Thread.sleep(1550); 
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
	  }	
  
	  
	  @Test(priority = 6)
	 public void MultiplerecordsSelectedfromDashboard() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
		    System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the BP name from Slidebar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on selected Bp Record from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on SELECTED BPRECORD2");
		    driver.findElement(By.xpath("//*[@id=\"sidenav_open_openBtn\"]")).click();
			Thread.sleep(1550); 
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
			
	  }	 
  
	  @Test(priority = 7)
	  public void OPENBLUEPRINTFROMSLIDEBARWITHVALIDINPUTS() throws Exception 
	  {
		  FileInputStream fi = new FileInputStream("config.properties");
			Properties prop= new Properties();
			prop.load(fi);
			System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
			WebDriver driver = new InternetExplorerDriver();
			driver.get(prop.getProperty("baseURL"));
			Thread.sleep(1550);
			Reporter.log("Browser Session Started");
		    driver.manage().window().maximize();
			Reporter.log("Browser Session Maximized");
			System.out.println(driver.getTitle());
		    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN_BUTTON)).click();
			Thread.sleep(1550);
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input")).sendKeys("Blue");
			Thread.sleep(1550);
			Reporter.log("Enter the BP name from Slidebar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/i")).click();
			Thread.sleep(1550);
			Reporter.log("Click on selected Bp Record from Slide bar");
			driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/label/i")).click();
			Thread.sleep(1550); 
			Reporter.log("Click on OPEN Button from Slide bar");
			driver.close();
			Reporter.log("Browser Session Closed");
			driver.quit();
	  }






}
