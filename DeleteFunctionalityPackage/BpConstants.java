package DeleteFunctionalityPackage;

public class BpConstants {
	
	  public static final String SLIDEBAR_NEW_BUTTON = "body > app-root > div > app-sidenav > div > div > div:nth-child(1) > div.sidenav-menu-btn-icon.sidenav-menu-btn-create";
	  public static final String SLIDEBAR_ENTERNAME_BPNAME = "body > app-root > div > app-sidenav > div > div.sidenav-action-cont > div.sidenav-action-body.custom-scrollbar > div:nth-child(1) > input";
	  public static final String SLIDE_CREATE_BUTTON = "body > app-root > div > app-sidenav > div > div.sidenav-action-cont > div.sidenav-action-body.custom-scrollbar > div:nth-child(1) > div.action-group-btn-cont > button";
	  //-----CreateBPRECORDSLIDE-----------
	  public static final String SLIDEBAR_OPEN_BUTTON = "/html/body/app-root/div/app-sidenav/div/div[1]/div[2]/div[1]";
	  public static final String SLIDEBAR_SEARCH_BLUEPRINTS = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/input";
	  public static final String SLIDEBAR_CLICK_BPRECORD= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[4]/label";
	  public static final String SLIDEBAR_CLICKON_OPENBUTTON = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[8]/button"; 
	  //----OPENBPRECORDSLIDEBAR------
	  public static final String SLIDEBAR_OPEN1_BUTTON = "/html/body/app-root/div/app-sidenav/div/div[1]/div[2]/div[1]";
	  public static final String SLIDEBAR_SEARCH1_BLUEPRINTS = "body > app-root > div > app-sidenav > div > div.sidenav-action-cont > div.sidenav-action-body.custom-scrollbar > div > div.search-box > input";
	  public static final String SLIDEBAR_CLICK1_BPRECORD1= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[2]/label";
	  public static final String SLIDEBAR_CLICK2_BPRECORD2= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[3]/label";
	  public static final String SLIDEBAR_CLICK3_BPRECORD3= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[4]/label";
	  public static final String SLIDEBAR_CLICKON2_OPENBUTTON2 = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[8]/button";
	  //---MultipleSelectRecords---------  
	  public static final String SLIDEBAR_OPEN2_BUTTON = "/html/body/app-root/div/app-sidenav/div/div[1]/div[2]/div[1]";
	  public static final String SLIDEBAR_SEARCH2_BLUEPRINTS = "body > app-root > div > app-sidenav > div > div.sidenav-action-cont > div.sidenav-action-body.custom-scrollbar > div > div.search-box > input";
	  public static final String SLIDEBAR_CLICK11_BPRECORD11= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[2]/label";
	  public static final String SLIDEBAR_CLICK22_BPRECORD22= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[3]/label";
	  public static final String SLIDEBAR_CLICK33_BPRECORD33= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[4]/label";
	  public static final String SLIDEBAR_Unselect_BPRECORD33= "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[7]/div[4]/label";
	  public static final String SLIDEBAR_CLICKON22_OPENBUTTON22 = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[8]/button";
	  //---Unselect the BPrecords from Dashboard----
	  public static final String SLIDEBAR_HELP_LINK = "/html/body/app-root/div/app-sidenav/div/div/div[4]/div[1]";
	  public static final String SLIDEBAR_HELPLINK_CLOSE = "help-cont-close-btn";
	  //----Help LINK FROM DASHBOARD----------
	  public static final String CREATEBP_LINK_QUICKHELP = "quickHelpCreateBlueprint";
	  public static final String ENTERNAMEBP_QUICK_HELP = "newBlueprintPopup_newName_Input";
	  public static final String CLICK_DONE_BUTTON_QUICKHELP= "newBlueprintPopup_Create_Button";
	  public static final String CLICK_OPEN_BUTTON = "newBlueprintPopup_OpenCreated_Button";
	  //----------CREATE NEW BLUEPRINT FROM QUICKHELP Links----------
	  public static final String CLICK_ON_BROWSE_BUTTON = "#yamlFileImportInput";
	  public static final String CLICK_ON_UPLOAD_BUTTON = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/div[2]/button"; 
	  //----Upload YAML FILE---------
	  public static final String CLICK_ON_DELETE_BUTTON = "table-btn-delete";
	  public static final String CLICK_ON_POP_UP_DELETE_BUTTON = "deleteBlueprintPopup_delete_Button";
	  public static final String CLICK_ON_OKAY_BUTTON = "deleteBlueprintPopup_okay_Button";
	  //-----------DELETE RECORD FROM DASHBOARD--------
	  public static final String CLICK_ON_CLONEBUTTON_DASHBOARD = "table-btn-copy";
	  public static final String ENTER_NAME_CLONEPOPUP_TEXTBOX = "#mat-dialog-3 > app-clone-dialog > div > div.dialog-body > div > div.dialog-input-box > input";
	  public static final String CLICK_ON_CLONE_BUTTON = "btn action-btn";
	  public static final String CLICK_ON_CLONEPOPUPOKAY_BUTTON = "#mat-dialog-2 > app-clone-dialog > div > div.dialog-body > div > div.dialog-actions > button.btn.action-btn.success-btn";
	  //--------CLONEARECORDFROMDAHBOARD----------
	  public static final String ENTER_TEXT_SEARCH_TEXTBOX = "toolbar_searchBox_input";
	  public static final String CLICK_ON_SEARCH_ICON = "/html/body/app-root/div/div/div/app-toolbar/div/div[2]/div[1]/button/i";
	  public static final String CLICK_ON_MAXIMIZE_BUTTON= "advSerch_Popup_maxBtn";
	  public static final String SELECT_AUTHOR_DROPDOWN = "authorName";
	  public static final String SELECT_FROM_DATE= "fromDate";
	  public static final String SELECT_TO_DATE= "toDate";  
	  public static final String CLICK_AWAITING_APPROVAL = "mat-slide-toggle-thumb";
	  public static final String CLICK_ON_APPLY_BUTTON = "advSerch_Filter_ApplyBtn";
	  //----Advance Search-------
	  public static final String CLICK_ON_BPSUMMARY_BUTTON = "blueprintSummaryChartDuration";
	  //----BLUEPRINT SUMMARY----------
	  public static final String CLICK_ON_CREATEBUTTON_DASHBOARD_BUTTON = "quickHelpCreateBlueprint";
	  public static final String ENTER_THE_BPNAME = "dialog-input ng-pristine ng-invalid ng-touched";
	  public static final String CLICK_ON_CREATE_BUTTON = "newBlueprintPopup_Create_Button";
	  public static final String CLICK_ON_OPEN_BUTTON = "newBlueprintPopup_OpenCreated_Button";
	  //-----Clone BP from Dashboard--------
	  public static final String CLICK_ON_CLONEBUTTON_DASHBOARD_BUTTON = "table-btn-copy";
	  public static final String ENTER_THE_NEWBPNAME_CLONEBP = "dialog-input-box";
	  public static final String CLICK_ON_CLONE_BUTTON_DASH = "btn action-btn";
	  public static final String CLICK_ON_OPEN_BUTTON_CLONE_DASH = "btn action-btn success-btn";
	  //-----Click on Weeks Button---------
	  public static final String CLICK_ON_LASTWEEK_ICON = "body > app-root > div > div > div > div > app-dashboard > div > div.dashboard-table-header > div.dashboard-table-header-timeline > button:nth-child(1)";
	  public static final String CLICK_ON_LAST30DAYS_ICON = "body > app-root > div > div > div > div > app-dashboard > div > div.dashboard-table-header > div.dashboard-table-header-timeline > button:nth-child(2)";
	  public static final String CLICK_ON_LAST6MONTHS_ICON= "body > app-root > div > div > div > div > app-dashboard > div > div.dashboard-table-header > div.dashboard-table-header-timeline > button:nth-child(3)";
	  //-----SORT RECORDS BY NAME-------
	  public static final String CLICK_ON_UPARROW_NAME = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[1]/i";
	  public static final String CLICK_ON_DOWNARROW_NAME = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[2]/i";
	  //-----SORT RECORDS CREATEDBY-------
	  public static final String CLICK_ON_UPARROW_NAME1 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[2]/i";
	  public static final String CLICK_ON_DOWNARROW_NAME1 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[2]/i";
	//-----SORT RECORDS CREATEDDATE-------
	  public static final String CLICK_ON_UPARROW_NAME2 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[3]/i";
	  public static final String CLICK_ON_DOWNARROW_NAME2 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[3]/i";
	//-----SORT RECORDS LastUpdatedBy-------
	  public static final String CLICK_ON_UPARROW_NAME3 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[4]/i";
	  public static final String CLICK_ON_DOWNARROW_NAME3 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[4]/i";
	//-----SORT RECORDS ModifiedDate-------
	  public static final String CLICK_ON_UPARROW_NAME4 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[5]/i";
	  public static final String CLICK_ON_DOWNARROW_NAME4 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[5]/i";
	//-----SORT RECORDS BySTATUS-------
	  public static final String CLICK_ON_UPARROW_NAME5 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[6]/i";
	  public static final String CLICK_ON_DOWNARROW_NAME5 = "/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/thead/tr/th[6]/i";
	  //--------Click on CANCEL Button on Clone Blueprint Popup----------
	  public static final String CLICK_ON_CLONEBUTTON_DASHBOARD_BUTTON1 = "table-btn-copy";
	  public static final String ENTER_THE_NEWBPNAME_CLONEBP1 = "cloneBlueprintPopup_cloneName_Input";
	  public static final String CLICK_ON_CLONE_BP_CANCEL = "cloneBlueprintPopup_cancelClone_Button";
	  //--------Search Action in Blueprint Editor----------
	  public static final String NAVIGATE_BLUEPRINT_EDITOR = "//div[normalize-space()='Template'][@tabindex='0']";
	  public static final String SEARCH_TEXT_FIELD = "toolbar_searchBox_input";
	  public static final String CLICK_ON_SEARCH_BUTTON_BPEDITOR = "toolbar_searchBox_btn";
	  public static final String CLICK_ON_TOGGLE_LINK0 = "advSerch_Status_Created";
	  public static final String CLICK_ON_TOGGLE_LINK1 = "advSerch_Status_Validate";
	  public static final String CLICK_ON_TOGGLE_LINK2 = "advSerch_Status_AwaitingApproval";
	  public static final String CLICK_ON_TOGGLE_LINK3 = "advSerch_Status_Approved";
	  public static final String CLICK_ON_TOGGLE_LINK4 = "advSerch_Status_Publish";
	  public static final String SELECT_CHECKBOX_ADVSEARCH = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-searchdialog/app-search-blueprint/div/div[2]/div[2]/div[1]/app-dashboardtable/div/table/tbody/tr[1]/td[1]/i";	  
	  public static final String CLICK_OPEN_ADVSEARCH = "advSerch_Action_OpenBtn";
	  public static final String SELECT_AUTHOR1_DROPDOWN = "/html/body/app-root/div/div/div/div/app-search-blueprint/div/div[2]/div[1]/form/div[2]/ng-select";
	  public static final String CLICK_ON_RECORD0 = "/html/body/app-root/div/div/app-yaml-editor/div/div[2]/div[1]/div/div[1]/div[1]";
	  public static final String CLICK_ON_RECORD1 = "/html/body/app-root/div/div/app-yaml-editor/div/div[2]/div[1]/div/div[1]/div[2]";
	  public static final String CLICK_ON_RECORD2 = "/html/body/app-root/div/div/app-yaml-editor/div/div[2]/div[1]/div/div[1]/div[3]";
	  public static final String CLICK_ON_RECORD3 = "/html/body/app-root/div/div/app-yaml-editor/div/div[2]/div[1]/div/div[1]/div[4]";
	  //----------CanceltheAdvancesearchwindow------------
	  public static final String CLICK_CANCEL_ADVSEARCH = "advSerch_Action_CancelBtn";
	  //Advance Search in Editor BTN icon
	  public static final String  CLICK_ON_SEARCH_ICON_IN_EDITOR="toolbar_searchBox_btn ";
      //-----Delete the BP record from Advance search window---------
	  public static final String CLICK_ONDELETEADV_BUTTON = "table-btn-delete";
	  public static final String CLICK_ONDELETE_BUTTON_ADVSEARCH = "btn action-btn";
	  public static final String CLICK_ONOKAY_BUTTON_ADVSEARCH = "btn action-btn success-btn";
	  //-----Delete the BP record from Advance search window with Cancel action---------
	  public static final String CLICK_ONCANCELADV_BUTTON = "advSerch_Action_CancelBtn";
	  //---------Clone BP Record from ADV SEARCH Window-------
	  public static final String CLICK_ON_CLONEBUTTON_ADVSEARCH = "/html/body/app-root/div/div/div/div/app-search-blueprint/div/div[2]/div[2]/div[1]/app-dashboardtable/div/table/tbody/tr[4]/td[9]/span[1]/i";
	  public static final String ENTER_THE_NAME_CLONEBLUPRINT_POPUP = "cloneBlueprintPopup_cloneName_Input";
	  public static final String CLICK_ONCLONE_BUTTON_ADVSEARCH_CLONEBLUPRINT_POPUP = "cloneBlueprintPopup_Clone_Button";
	  public static final String CLICK_ONOPEN_BUTTON_ADVSEARCH_CLONEBLUPRINT_POPUP = "cloneBlueprintPopup_openCloned_Button";
	  //---------CANCEL Clone BP Record from ADV SEARCH Window-------
	  public static final String CLICK_ON_CANCELCLONEBUTTON_ADVSEARCH = "cloneBlueprintPopup_cancelClone_Button";
	  //--------Wildcardsearch----------
	  public static final String CLICK_ON_CLEAR_BUTTON = "advSerch_Filter_ClearBtn";
	  public static final String CLICK_ON_CANCEL_BUTTON = "advSerch_Action_CancelBtn";
	  //---------Click on BP record from Dashboard-------
	  public static final String CLICK_ON_CREATEDBPRECORD_DASHBOARD = "/html/body/app-root/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/tbody/tr[1]/td[1]";
	  public static final String CLICK_ON_CREATEDBPRECORD1_DASHBOARD= "/html/body/app-root/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/tbody/tr[2]/td[1]";
	  public static final String CLICK_ON_CREATEDBPRECORD2_DASHBOARD= "/html/body/app-root/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/tbody/tr[3]/td[1]";
	  public static final String CLICK_ON_RETURNTO_DASHBOARD = "/html/body/app-root/div/div/div[2]/app-toolbar/div/div[1]/div[1]";
	  public static final String CLICK_ON_CREATEDBPRECORD3_DASHBOARD= "//table[@class='dashboard-table custresize']//tr[3]//td[1]";
	  public static final String CLICK_ON_CREATEDBPRECORD4_DASHBOARD= "//table[@class='dashboard-table custresize']//tr[4]//td[1]";
	  public static final String CLICK_ON_CREATEDBPRECORD5_DASHBOARD= "//table[@class='dashboard-table custresize']//tr[5]//td[1]";
	  public static final String CLICK_ON_CREATEDBPRECORD6_DASHBOARD= "//table[@class='dashboard-table custresize']//tr[6]//td[1]";
	  public static final String CLICK_ON_ZOOMBUTTON_BPEDITOR= "//a[@class='action-item zoom-in']";
	  public static final String CLICK_ON_ZOOMOUTBUTTON_BPEDITOR = "//a[@class='action-item zoom-auto']";
	  public static final String CLICK_ON_AUTOZOOMBUTTON_BPEDITOR = "//a[@class='action-item zoom-out']";
	  public static final String CLICK_ON_AUTODRAWBUTTON_BPEDITOR= "//a[@class='action-item zoom-out']";
      //-------------Admin Menu File Upload---------------
	  public static final String CLICK_ON_ADMIN_BUTTON = "/html/body/app-root/div/app-sidenav/div/div[1]/div[3]/div[2]";
	  public static final String CLICK_ON_BROWSE_BUTTON_ADMIN = "adminExcelFileInput";
	  public static final String CLICK_ON_UPLOAD_BUTTON_ADMIN = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[2]/div[2]/button";
	  //-------Click on Cancel button from Delete Blueprint POPUP Dashboard-----------
	  public static final String CLICK_ON_DELETE_BUTTON_DASHBOARD_POPUP = "/html/body/app-root/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/tbody/tr[1]/td[8]/span[2]";
	  public static final String CLICK_ON_CANCEL_BUTTON_DASHBOARD_POPUP = "cloneBlueprintPopup_cancelClone_Button";
	  //-------Click on Cancel Button from Clone Blueprint pop up-----------
	  public static final String CLICK_ON_CLONE_BUTTON_DASHBOARD_POPUP = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-searchdialog/app-search-blueprint/div/div[2]/div[2]/div[1]/app-dashboardtable/div/table/tbody/tr[60]/td[9]/span[1]/i";
	  public static final String CLICK_ON_CANCEL_BUTTON_DASHBOARD_POPUP2 = "cloneBlueprintPopup_cancelClone_Button";
	  //-------Click on Close Button from Clone Blueprint pop up-----------
	  public static final String CLICK_ON_CLOSE_BUTTON_DASHBOARD_POPUP2 = "cloneBlueprintPopup_cancelClone_Button";
	  //------------BluePrintSummary-----------
	  public static final String CLICK_ON_BLUEPRINTSUMMARY_THISWEEK = "blueprintSummaryChartDuration";	  
	  //------------BluePrintSummary-----------
	  public static final String CLICK_ON_PUBLISHEDVERSUSCREATED_THISWEEK = "publishedVsCreatedChartDuration";
	  //-----------Close the Advance search Popup------------
	  public static final String CLICK_ON_CLOSE_BUTTON_ADVSEARCHPOPUP = "header-action-btn";
	  //---click on CANCEL Button from CreatedBlueprint popup----------
	  public static final String CLICK_ON_CANCEL_BUTTON_DASHBOARDCREATEDBP_POPUP = "newBlueprintPopup_Cancel_OpenCreated_Button";
	  //---click on CANCEL Button from CreatedBlueprint popup----------
	  public static final String CLICK_ON_CLOSE_BUTTON_DASHBOARDCREATEDBP_POPUP = "dialog-close-btn";
	  //----------Select ALL records------------
	  public static final String SELECT_ALL_RECORDS = "//table[@class='dashboard-table custresize']//tr[1]//th[1]";
      //---Edit yaml file-----------	  
	 public static final String Edit_Yaml_file = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/div[2]/div[1]/button[1]";
	 //---Delete Yaml File-------
	 public static final String DELETE_Yaml_file = "/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div[3]/div[2]/div[1]/button[2]";
	 //-------------YangfileImportsBlueprintEditor----------
	 public static final String CLICK_ONBLUEPRINT_EDITOR = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[1]/div[1]/a[1]";
	 //--------Navigate to Import Yang File------------
	 public static final String CLICK_ON_IMPORT_YANG_FILE = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[1]/div[1]/a[2]";
     //----------Enter the File Name Import yang----------
	 public static final String ENTER_THE_FILE_NAME = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/app-yang-import/div/div[1]/div[2]/input";
     //--------Click on Open BLUEPRINT RECORD----------
	 public static final String Open_BPRECORD = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/app-yang-import/div/div[1]/div[3]/div/div[2]/button";
	 //------Click on NCS Mapping Record---------
	 public static final String NCS_MAPPING_RECORD = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[1]/ul/li[1]/ul/li/div/i";
	 //-------Move to Current Blueprint Inputs--------
	 public static final String Current_Blueprint_Inputs = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[2]/div/div[1]/i";
     //--------Click on Next Button--------
	 public static final String Current_Blueprint_Inputs_NEXT_BUTTON = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[2]/div[2]/button";
     //Click on NODES IN NSD COnfiguration---------
	 public static final String Node_1= "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[2]/div[1]";
	 public static final String Node_2= "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[3]/div[1]";
	 public static final String Node_3= "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[4]/div[1]";
	 public static final String Node_4 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[5]/div[1]";
	 public static final String Node_5 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[6]/div[1]";
	 public static final String Node_6= "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[7]/div[1]";
	 public static final String Node_7 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[8]/div[1]";
	 public static final String Node_8 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[9]/div[1]";
	 //----Delete Node_1 in NSD Configuration--------
	 public static final String DELETE_1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[2]/div[2]/button[2]";
	 public static final String DELETE_UNDO_BUtton = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[2]/div[2]/button[2]";	 
	//Click on Interfaces NSD COnfiguration---------
	public static final String Interface_1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[2]/div[1]/i";
     public static final String Interface_2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[3]/div[1]/i";
	public static final String Interface_3= "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[4]/div[1]/i";
	public static final String Interface_4 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[5]/div[1]/i";
	public static final String Interface_5 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[6]/div[1]/i";
	//Click on OPERATIONS NSD COnfiguration---------
		public static final String operation_1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[3]/div[2]/div[1]/i";
	    public static final String operation_2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[3]/div[3]/div[1]/i";
		public static final String operation_3 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[3]/div[4]/div[1]/i";
		public static final String operation_4 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[3]/div[5]/div[1]/i";
		public static final String operation_5 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[3]/div[6]/div[1]/i";
		//Click on Implementation API---------
		public static final String API_1_EDIT = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/button[1]/i";
		public static final String API_DELETE_1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/button[2]";
		public static final String API_2_EDIT = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/button[1]";
		public static final String API_DELETE_2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/button[2]";
		public static final String Dependency_1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[1]/div[2]/div[1]/i";
		public static final String Dependency_2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[1]/div[3]/div[1]/i";
		public static final String Dependency_1_EDIT = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[1]/div[2]/div[2]/button[1]";
		public static final String Dependency_1_DELETE = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[1]/div[2]/div[2]/button[2]";
		public static final String Dependency_2_EDIT = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[1]/div[3]/div[2]/button[1]";
		public static final String Dependency_2_DELETE = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[1]/div[3]/div[2]/button[2]";
		//Click on Inputs NSD CONfiguration---------
				public static final String Input_1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[2]/div[1]/i";
				public static final String Input_2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[3]/div[1]/i";
				public static final String Input_3 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[4]/div[1]/i";
				public static final String INPUT_EDIT1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[2]/div[2]/button[1]";
				public static final String INPUT_EDIT2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[2]/div[3]/button[1]";
				public static final String INPUT_EDIT3 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[2]/div[4]/button[1]";
				public static final String INPUT_DLETE1 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[2]/div[2]/button[2]";
				public static final String INPUT_DLETE2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[3]/div[2]/button[2]";
				public static final String INPUT_DLETE3 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[1]/div[2]/div[4]/div[2]/button[2]";
				//Click on Inputs_11 NSD CONfiguration---------
				public static final String Input_11 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[2]/div[1]/i";
				public static final String Input_22 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[3]/div[1]/i";
				public static final String Input_33 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[4]/div[1]/i";
				public static final String INPUT_EDIT11 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[2]/div[2]/button[1]";
				public static final String INPUT_EDIT22 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[3]/div[2]/button[1]";
				public static final String INPUT_EDIT33 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[4]/div[2]/button[1]";
				public static final String INPUT_DLETE11 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[2]/div[2]/button[2]";
				public static final String INPUT_DLETE22 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[3]/div[2]/button[2]";
				public static final String INPUT_DLETE33 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[4]/div[2]/div[2]/div[4]/div[2]/button[2]";
          //------CLICK ON BACK Button----------
				public static final String BACK_BUTTON = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[2]/div[2]/button[1]";
			//------CLICK ON BACK Button----------
				public static final String Confirm_BUTTON = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[2]/div[2]/button[2]";
        //------Click on Edit button in NSD Configuration----------------
				public static final String EDIT_BUTTON = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[2]/div[2]/button[1]";
			     public static final String Enter_THE_EDIT_VALUE = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[2]/div[1]/input";
				public static final String CLICK_SAVE_BUTTON = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[1]/div[2]/div[2]/button";
				public static final String CLOSE_POP_UP = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[1]/button/i";
			//----------Click on Delete Interface-1---------
				public static final String EDIT_BUTTON_Interfaces = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[2]/div[2]/button[2]";
			     public static final String Enter_THE_EDITTEXT_VALUE_Interfaces = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[3]/div[1]/i";
				public static final String CLICK_SAVE_BUTTON_Interfaces = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-nsd-config/div/div[1]/div[2]/div[2]/div[2]/button[1]";
				public static final String CLOSE_POP_UP_Interfaces = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[1]/button/i";
				//---------------Click on CARD Status 5-----------
				public static final String Card_status_5 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[1]/ul/li[3]/ul/li[1]/div/i";
				public static final String Arrow_count = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[2]/div/div[1]";
				public static final String Confirm_button = "custom-btn btn-confirm ng-star-inserted";
				public static final String Ok_button = "/html/body/div[2]/div[4]/div/mat-dialog-container/app-alert-dialog/div/div[2]/button";
				//--------Click on Device Id-----------
				public static final String Click_Device_id = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[2]/div/i";
                //-----------Click on Find and Assign----------------
				public static final String Click_On_Find_and_Assign = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[3]/div/i";
				//-----------Click on Find and Assign----------------
				public static final String Click_On_Port_id = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[3]/div/i";
				//-----------Click on Find and Assign----------------
				public static final String Click_On_Order_id = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[2]/div/i";
				//-----------Click on Find and Assign----------------
				public static final String Click_On_Card_slot = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[1]/ul/li[4]/div/i";
				//-----------Click on Find and Assign----------------
				public static final String Click_On_Card_Type = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[1]/ul/li[5]/div/i";
				//-----------Click on Find and Assign----------------
				public static final String Click_On_Card_Status = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[1]/ul/li[6]/div/i";
				//-----------Click on Find and Assign----------------
				public static final String Card_Slot_Dummy_2 = "/html/body/div[2]/div[2]/div/mat-dialog-container/app-yang-file-import/div/div[3]/app-ncs-mapping/div/div[1]/div[1]/div[2]/div[1]/app-yang-tree/ul/li[1]/ul/li[1]/ul/li[3]/ul/li[2]/div/i";
                //-----------Click on Inputs----------
				public static final String Click_On_inputs = "/html/body/app-root/div/div/div[1]/div/app-editor-side-bar/div/div[3]/div/ol/li[2]/div/div[1]/span";
                //---------Click on Roots---------
				public static final String Click_On_Roots = "body > app-root > div > div > div.main-area > div > app-yaml-editor > div > div > div.page-section > app-input-editor-page > div > div.content.root.group.block > div.left > div > div.tree > app-input-tree > div > app-input-tree-node > div > a > span";
                //----------Enter the Name--------
				public static final String Enter_the_name = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[2]/app-input-tree-node-form/div/div/div/div[1]/div[3]/div[3]/input";
                //-----------Click on Add Blue Button---------
				public static final String Click_On_AddNewButton = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[2]/app-input-tree-node-form/div/div/div/div[1]/div[3]/div[3]/i";
                //---------Click on Apply Button--------
				public static final String Click_On_ApplyButton = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[2]/app-input-tree-node-form/div/div/div/div[2]/button[2]";
                //---------Click on BPEditor---------
				public static final String Click_on_BPEditor = "/html/body/app-root/div/div/div[2]/app-toolbar/div/div[1]/div[2]";
				//---------Click_On_inputs_Bpeditor------------
				public static final String Click_on_Inputs_BPEditor = "/html/body/app-root/div/div/div[1]/div/app-editor-side-bar/div/div[3]/div/ol/li[2]/div/div[1]/span";
				//-----------Click on Reset Button-----------
				public static final String Click_on_Reset_Button = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[2]/app-input-tree-node-form/div/div/div/div[2]/button[1]";
				//------Click on Add Inputs----------
				public static final String Click_on_Add_Inputs = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[1]/div[2]/button";
				//-------Enter the Name--------
				public static final String Enter_Inputs_Name = "body > app-root > div > div > div.main-area > div > app-yaml-editor > div > div > div.page-section > app-input-editor-page > div > div.content.input > div.left > div > div.fields-container > app-input-node-fields > div > div.name > div > input";
				//---------Click on Add Button in section1-------
				public static final String Click_add_Button_Inputs = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[2]/div/a/i";
                //---------Inputs Functionality-----------
				public static final String Click_on_AddInputs_BPEditor = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[1]/div[2]/button";
				public static final String Enter_The_Name = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[1]/div/input";
				public static final String Click_on_Section1 = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[2]/div/a/i";
				public static final String Enter_the_Type = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[2]/div[2]/div[1]/div[1]/div/input";
				public static final String Enter_the_Description = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[2]/div[2]/div[2]/div/div[2]/textarea";
				public static final String Enter_the_Display_Name = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[2]/div[2]/div[1]/div[2]/div[2]/input";
				public static final String Click_on_Section2 = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div/a/i";
				public static final String Click_on_True_Required = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[1]/div[2]/app-input-boolean-api-call/div/div/div/a[1]/div[1]";
				public static final String Click_on_False_Required = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[1]/div[2]/app-input-boolean-api-call/div/div/div/a[2]/div[1]";
				public static final String Click_on_Apicall_Required = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[1]/div[2]/app-input-boolean-api-call/div/div/div/a[3]/div[1]";
				public static final String Click_on_True_Display = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[2]/div[2]/app-input-boolean-api-call/div/div/div/a[1]/div[1]";
				public static final String Click_on_False_Display = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[2]/div[2]/app-input-boolean-api-call/div/div/div/a[2]/div[1]";
				public static final String Click_on_Apicall_display = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[2]/div[2]/app-input-boolean-api-call/div/div/div/a[3]/div[1]";
				public static final String Click_on_True_Editable = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[3]/div[2]/app-input-boolean-api-call/div/div/div/a[1]/div[1]";
				public static final String Click_on_False_Editable = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[3]/div[2]/app-input-boolean-api-call/div/div/div/a[2]/div[1]";
				public static final String Click_on_apicall_Editable = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[3]/div[2]/div[3]/div[2]/app-input-boolean-api-call/div/div/div/a[3]/div[1]";
				public static final String Click_on_Section3 = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div/a/i";
				public static final String Fx_get_input = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[1]/div[1]/i[1]";
                public static final String Click_On_In_project_name = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/div[1]/ng-select/ng-dropdown-panel/div/div[2]/div[1]/span";
                public static final String Fx_get_property = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[1]/div[2]/i[1]";
                public static final String Click_On_Create_Scheme_node = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/div[1]/ng-select[1]/ng-dropdown-panel/div/div[2]/div[2]/span";
                public static final String Click_On_New_equipment = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/div[1]/ng-select[2]/ng-dropdown-panel/div/div[2]/div[1]/span";
                public static final String Click_On_project_id = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/div[1]/ng-select[3]/ng-dropdown-panel/div/div[2]/div[2]/span";
				public static final String Fx_Constant = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[1]/div[4]/i[1]";
				public static final String Enter_The_Input_constant = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/input";
				public static final String Fx_Webservice = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[1]/div[5]/i[1]";
                public static final String Method_name2 = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div/div[2]/div[2]/i";
                public static final String Parameter_Method_name1 = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/div[2]/ng-select/ng-dropdown-panel/div/div[2]/div[1]/span";
				public static final String Fx_Uiservice = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[1]/div[6]/i[1]";
                public static final String click_on_method_name_String = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div/div[2]/div[2]/i";
                public static final String Click_on_methodName1 = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/div[2]/ng-select/ng-dropdown-panel/div/div[2]/div[1]/span";
                public static final String Click_on_InputList = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[1]/div[2]/div[3]/i";
				public static final String Click_on_In_project_type = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/app-inputs-outputs-item-select/div/div[1]/div[3]/div[1]/div/i";
				public static final String Click_on_In_snet_type = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/app-inputs-outputs-item-select/div/div[1]/div[3]/div[8]/div/i";
				public static final String Click_on_In_project_Sub_type = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/app-inputs-outputs-item-select/div/div[1]/div[3]/div[3]/div/i";
				public static final String Click_on_In_Card_model = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/app-inputs-outputs-item-select/div/div[1]/div[3]/div[42]/div/i";
				public static final String Click_on_In_project_id = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/app-inputs-outputs-item-select/div/div[1]/div[3]/div[57]/div/i";
				public static final String Click_on_Constarint_Right_arrow = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/app-inputs-outputs-item-select/div/div[2]/i[1]";
				public static final String Click_on_Constarint_left_arrow = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-webservice-uiservice-editor/div/div[2]/app-inputs-outputs-item-select/div/div[2]/i[2]";
				public static final String Click_on_API = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[1]/div[7]/i[1]";
				public static final String Click_on_get_from_xyz= "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[1]/div/div[2]/app-input-node-fields/div/div[4]/div[2]/div[1]/div[2]/app-value-expression-editor-v2/div/div[2]/app-input-boolean-api-call/div/div/ng-select/ng-dropdown-panel/div/div[2]/div[1]/span";
				public static final String Click_on_Constarint_Editor = "/html/body/app-root/div/div/div[2]/div/app-yaml-editor/div/div/div[2]/app-input-editor-page/div/div[2]/div[2]/div[1]/a/span";
				public static final String Click_on_Webservice = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[1]/div[3]/span";
				public static final String Click_on_Uiservice = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[1]/div[4]/span";
				public static final String Click_on_Functions = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[1]/div[5]/span";
				public static final String Click_on_Constraints = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[1]/div[2]/i";
				public static final String Click_on_Icon_Constraints_box = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/i";
				public static final String Click_on_greater_then = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[1]/div/div[2]/div[1]/div/app-constraint-level1/div/div/div/div/span";
				public static final String Click_on_Equals= "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[1]/div/div[2]/div[1]/div/app-constraint-level1/div/div[1]/div/ng-select/ng-dropdown-panel/div/div[2]/div[1]/span";
				public static final String Click_on_lessThen = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[1]/div/div[2]/div[1]/div/app-constraint-level1/div/div[1]/div/ng-select/ng-dropdown-panel/div/div[2]/div[3]/span";
				public static final String Click_on_greaterthenorEqual = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[1]/div/div[2]/div[1]/div/app-constraint-level1/div/div[1]/div/ng-select/ng-dropdown-panel/div/div[2]/div[4]/span";
				public static final String Click_on_Add_purple = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[1]/div/div[2]/div[1]/div/app-constraint-level1/div/div/div/i";
				public static final String Click_on_Icon_setting_blue= "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[1]/div/div[2]/div[1]/div/app-constraint-level1/div/div/div[2]/div[2]/i";
				public static final String Click_on_Get_input = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[2]/div/app-value-expression-editor-v2/div/div[2]/ng-select/ng-dropdown-panel/div/div[2]/div[1]/span";
				public static final String Click_on_Project_name = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[2]/app-constraint-editor-v2/div/div[2]/div[2]/div/app-value-expression-editor-v2/div/div[4]/div[1]/ng-select/ng-dropdown-panel/div/div[2]/div[1]/span";
				public static final String Click_on_Close_button = "/html/body/ngb-modal-window/div/div/app-constraint-editor-v2-modal/div/div[1]/i";
                //---------DashBoardEndtoEnd------------------------
				public static final String Click_on_the_BP_RECORD = "body > app-root > div > div > div > div > app-dashboard > div > div.dashboard-table-body > app-dashboardtable > div > table > tbody > tr:nth-child(6) > td.td-bp-name";

                 
				
				
				
                
				
				
				
				
				
				
				
				
				
				
				

				
                
                
                
                
                
                
                
                
                
                
                
                
                
                
				
				
				
				
				
				
				


				
				
				
				
				


	 
	 
	 
	 
	 
	 

	 


	  


	  

	  
	  
	  
	  
	  
	  
	  

	  
	  
	  

	  
	  

	  
			  
	  
	  
     

	  
	  
	  
	  
	  
	  
	 


	  

	  
	  


	  

      
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  

	  
	  


}
