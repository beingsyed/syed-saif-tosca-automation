package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ClickontheBlueprintRecordDashboard {
  @Test
  public void ClickontheBlueprintRecordDashboard1() throws Exception 
  {

	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
        System.out.println(driver.getTitle());
		Thread.sleep(1550);
		Reporter.log("Browser Session Maximized");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.xpath("//table[@class='dashboard-table custresize']//tr[1]//td[1]")).click();
	    Thread.sleep(1550);
		Reporter.log("Click on the BpRecord1 dashboard");
	    System.out.println(driver.getTitle());
	    Thread.sleep(1550);
	    driver.findElement(By.xpath("//div[@class='item'][normalize-space()='Dashboard']")).click();
	    Thread.sleep(1550);
		Reporter.log("Click on the Returnto dashboard Tab");
	    System.out.println(driver.getTitle());
	    Thread.sleep(1550);
	    driver.findElement(By.xpath("/html/body/app-root/div/div/div/div/app-dashboard/div/div[2]/app-dashboardtable/div/table/tbody/tr[45]/td[1]")).click();
	    Thread.sleep(1550);
		Reporter.log("Click on the BpRecord2 dashboard");
		
		Thread.sleep(1550);		
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();
		
	  
  }
  

}
