package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class CancelButtoninDELETEPOPUP {
  @Test
  public void ClickonCancelButton() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Thread.sleep(2550);
		Reporter.log("Browser Maximized");
		driver.findElement(By.xpath("//td[normalize-space()='test2']/following-sibling::td/span[@class='table-btn-delete']")).click();
		Thread.sleep(2550);
		Reporter.log("Click on Delete Button");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.id("deleteBlueprintPopup_cancelDelete_Button")).click();
	    Thread.sleep(1550);
		Reporter.log("Click on Okay");
	    System.out.println(driver.getTitle());
	    driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();

	    
  }
 
}
