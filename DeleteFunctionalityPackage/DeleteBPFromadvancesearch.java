package DeleteFunctionalityPackage;


import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class DeleteBPFromadvancesearch {
  @Test
  public void DeleteBPADVRecord() throws Throwable 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    driver.findElement(By.xpath("//div[@class='quick-create-bp']")).click();
		Thread.sleep(1550);
		Reporter.log("click on new template");
		driver.findElement(By.xpath("//input[@id='newBlueprintPopup_newName_Input']")).sendKeys("delete1");
		Thread.sleep(1550);
		Reporter.log("click on new template");
		driver.findElement(By.xpath("//button[@class='btn action-btn']")).click();
		Thread.sleep(1550);
		Reporter.log("click on new template");
		driver.findElement(By.xpath("//button[@class='btn cancel-btn']")).click();
		Thread.sleep(1550);
		Reporter.log("click on new template");
		driver.findElement(By.xpath("//td[normalize-space()='delete1']/following-sibling::td/span[@class='table-btn-delete']")).click();
		Thread.sleep(1550);
		Reporter.log("click on new template");
		driver.findElement(By.xpath("//button[@class='btn action-btn']")).click();
		Thread.sleep(1550);
		Reporter.log("click on new template");
		driver.findElement(By.xpath("//button[@class='btn action-btn success-btn']")).click();
		Thread.sleep(1550);
		Reporter.log("click on new template");
		driver.close();
		driver.quit();
		
  }
 

}
