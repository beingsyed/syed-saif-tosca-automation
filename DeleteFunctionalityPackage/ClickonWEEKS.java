package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ClickonWEEKS {
  @Test(priority =11)
  public void ClickonWeekdbuttonDashboard() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
		Reporter.log("Browser Session maximized");
		Thread.sleep(1550);
		driver.findElement(By.cssSelector(BpConstants.CLICK_ON_LASTWEEK_ICON)).click();
		Thread.sleep(1550);
		Reporter.log("Click on Lastweeks link");
		driver.findElement(By.cssSelector(BpConstants.CLICK_ON_LAST30DAYS_ICON)).click();
		Thread.sleep(1550);
		Reporter.log("Click on Last30days link");
		driver.findElement(By.cssSelector(BpConstants.CLICK_ON_LAST6MONTHS_ICON)).click();
		Reporter.log("Click on Last6monthsLink");
		Thread.sleep(1550);
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit();

	  
  }
  

}
