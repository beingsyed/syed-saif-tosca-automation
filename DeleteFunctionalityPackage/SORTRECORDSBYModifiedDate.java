package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SORTRECORDSBYModifiedDate {
  @Test
  public void SORTRECORDSByLASTMODIFIEDDATE() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Reporter.log("Browser Session Started");
		Thread.sleep(1550);
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Reporter.log("Browser Session maximized");
	    driver.findElement(By.xpath(BpConstants.CLICK_ON_UPARROW_NAME4)).click();
		Thread.sleep(1550);
		Reporter.log("CLICK ON UPARROW BUTTON");
	    driver.findElement(By.xpath(BpConstants.CLICK_ON_DOWNARROW_NAME4)).click();
		Reporter.log("CLICK ON DOWNARROW BUTTON");
		Thread.sleep(1550);
		driver.close();
		Reporter.log("Browser Session Closed");
		driver.quit(); 
  }
  
 

}
