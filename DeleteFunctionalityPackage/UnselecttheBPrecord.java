package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class UnselecttheBPrecord {
  @Test
  public void UnselecttheSelectedRecord() throws Exception
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
		Reporter.log("Browser Session Maximized");
	    System.out.println(driver.getTitle());
	    driver.findElement(By.xpath(BpConstants.SLIDEBAR_OPEN2_BUTTON)).click();
		Thread.sleep(1550);
		Reporter.log("CLICK ON OPEN  Button");
		driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_SEARCH2_BLUEPRINTS)).sendKeys("test");
		Thread.sleep(1550);
		Reporter.log("Enter the Blueprint Name");
	   	driver.findElement(By.xpath("/html/body/app-root/div/app-sidenav/div/div[2]/div[2]/div/div[3]/div[1]/div[1]/label/span")).click();
		Thread.sleep(1550);
		Reporter.log("Click on the Blueprint Record");
		driver.findElement(By.xpath("//button[normalize-space()='Open']")).click();
		Thread.sleep(1550);
		Reporter.log("Open the Selected BluePrint");
		driver.close();
		Reporter.log("BROWSER SESSION CLOSED");
		driver.quit();
  }
 

}
