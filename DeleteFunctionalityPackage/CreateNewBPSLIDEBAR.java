package DeleteFunctionalityPackage;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class CreateNewBPSLIDEBAR {
  @Test
  public void CreateNewBlueprintrecordfromSlidebar() throws Exception
  {

	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Reporter.log("Browser Session Maximized");
	    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_NEW_BUTTON)).click();
		Thread.sleep(1550);
		Reporter.log("Click on SLIDE BAR NEW BUTTON");
	    driver.findElement(By.cssSelector(BpConstants.SLIDEBAR_ENTERNAME_BPNAME)).sendKeys("DelAutoScript4");
		Thread.sleep(1550);
		Reporter.log("Enter the BP NAME  on SLIDE BAR CREATE BUTTON");
	    System.out.println(driver.getTitle());
		driver.findElement(By.cssSelector(BpConstants.SLIDE_CREATE_BUTTON)).click();
		Thread.sleep(2550);
		Reporter.log("Click on SLIDE BAR CREATE BUTTON");
	    System.out.println(driver.getTitle());
	    driver.close();
		Reporter.log("Browser Session Closed");
	    driver.quit();
		Reporter.log("Browser Session Quit");


	    
  }
  @Test(priority =2)
  public void ClickonCancelButtonBlueprintCreatedPopupDashboarddelete123456() throws Exception 
  {
	  FileInputStream fi = new FileInputStream("config.properties");
		Properties prop= new Properties();
		prop.load(fi);
		System.setProperty("webdriver.ie.driver",prop.getProperty("ieDriverPath") );
		WebDriver driver = new InternetExplorerDriver();
		driver.get(prop.getProperty("baseURL"));
		Thread.sleep(1550);
		Reporter.log("Browser Session Started");
	    driver.manage().window().maximize();
	    System.out.println(driver.getTitle());
		Thread.sleep(1550);
		Reporter.log("Browser Maximized");
		driver.findElement(By.xpath("//td[normalize-space()='DelAutoScript4']/following-sibling::td/span[@class='table-btn-delete']")).click();
	    System.out.println(driver.getTitle());
		Reporter.log("Click on delete Button");
		driver.findElement(By.xpath("//button[@class='btn action-btn'][normalize-space()='Delete']")).click();
	    System.out.println(driver.getTitle());
		Reporter.log("Click on delete Button");
		driver.findElement(By.xpath("//button[@class='btn action-btn success-btn'][normalize-space()='Okay']")).click();
	    System.out.println(driver.getTitle());
		Reporter.log("Click on okay Button");
		driver.close();
		Reporter.log("Browser Session Closed");
	    driver.quit();
  }

}
